import pymysql

from config import Config


def get_rdb_client(service_region, config_table_name, config_table_primary_key, config_table_record_id):
    """
    This method creates an rdb client that can be used to perform database 
    operations.

    Args:
        service_region (str): The AWS Service Region. e.g. us-west-2
        config_table_name (str): Name of configuration table storing the 
                                 encrypted configs
        config_table_primary_key (str): Name of primary key for the 
                                        configuration table
        config_table_record_id (str): The primary key for the record you want 
                                      the configugration details for. In this
                                      case this is the configuration for the 
                                      MySQL credentials.

    Return:
        Returns a mysql db client.
    """
    config_result = Config.get_config(service_region, config_table_name, 
        config_table_primary_key, config_table_record_id)

    db_name = config_result["db_name"]
    db_host_endpoint = config_result["db_host_endpoint"]
    db_port = int(config_result["db_port"])
    db_username = config_result["db_username"]
    db_password = config_result["db_password"]

    db_connection = pymysql.connect(db_host_endpoint, user=db_username, passwd=db_password, db=db_name, port=db_port, connect_timeout=5, charset='utf8')

    return db_connection


def check_table_exists(db_connection, table_name):
    """
    This method checks to see if the given table exists within the provided db

    Args:
        db_connection (obj): The mysql database connnection object
        table_name (str): The name of the table you want to look for

    Returns:
        boolean: True if the table exists, False if not.
    """
    table_exists = False

    db_cursor = db_connection.cursor()
    db_cursor.execute("""
        SELECT COUNT(*) 
        FROM information_schema.tables 
        WHERE table_name = '{0}'
    """.format(table_name.replace('\'', '\'\'')))

    if db_cursor.fetchone()[0] == 1:
        table_exists = True

    db_cursor.close()
    return table_exists

def build_create_table_string(table_configuration):
    """
    Creates the string that can be used to create tables in the sql db.

    Args:
        table_config (obj): Object containing the table configuration details
                            coming from the lambda.

    Returns:
        str: The string needed to create the given sql table
    """
    create_string = "CREATE TABLE " + table_configuration["sql_table_name"] + "("

    field_list = []
    for field in table_configuration["fields"]:
        field_list.append(field["sql_field_name"] + " " + field["sql_field_properties"])

    field_string = ",".join(field_list)

    create_string += field_string + ",PRIMARY KEY (Id))"

    return create_string

def build_upsert_string(table_configuration):
    """
    Creates the string that can be used to upsert records in the sql db. This 
    string does not include values, but contains a {} placeholder that can be
    used with format to replace the actual values.

    Args:
        table_config (obj): Object containing the table configuration details
                            coming from the lambda.

    Returns:
        str: string with empty values that can be replaced in order to do the
             actual database upsert.


        INSERT INTO submarket
            (Id, Name, Active__c, Expedia_Sub_Market_Id__c, Market__c, Region__c, Super_Region__c)
        VALUES
            {0}
        ON DUPLICATE KEY UPDATE
            Id = VALUES(Id),
            Name = VALUES(Name),
            Active__c = VALUES(Active__c),
            Expedia_Sub_Market_Id__c = VALUES(Expedia_Sub_Market_Id__c),
            Market__c = VALUES(Market__c),
            Region__c = VALUES(Region__c),
            Super_Region__c = VALUES(Super_Region__c)
    """
    upsert_string = "INSERT INTO " + table_configuration["sql_table_name"] + " ("

    field_list = []
    update_list = []
    for field in table_configuration["fields"]:
        field_list.append(field["sql_field_name"])
        update_list.append(field["sql_field_name"] + " = VALUES(" + field["sql_field_name"] + ")")

    field_list_string = ",".join(field_list)
    update_list_string = ",".join(update_list)

    upsert_string += field_list_string + ") VALUES {0} ON DUPLICATE KEY UPDATE " + update_list_string

    return upsert_string