import os
import sys
sys.path.append("{}/pip_libs".format(os.path.dirname(os.path.realpath(__file__))))
import traceback
import json
import ast
from datetime import datetime

import pymysql
import boto3

from config import Config
import sfdc_helper
import database_helper
import pysalesforce
import webservice


def lambda_handler(event, context):
    if 'detail-type' in event and event.get('detail-type') == 'Scheduled Event':
        print(event.get('detail-type'))
        return

    if 'body' in event:
        event = json.loads(event['body'])

    print(event)
    print(type(event))

    #Retrives the Body from json string when this lambda is initated from SQS
    if 'Records' in event:
        message_body = event.get('Records')[0].get('Sns').get('Message')
        print(type(message_body))
        event = json.loads(message_body)
        if type(event) == str:
            print('unicode')
            message_body = ast.literal_eval(message_body)
        event = json.loads(message_body)
    print(event)

    service_region = os.environ["service_region"]
    config_table_name = os.environ["config_table_name"]
    config_table_primary_key = os.environ["config_table_primary_key"]
    db_config_id = os.environ['db_config']
    sf_config_id = os.environ["sf_config"]

    try:
        return create_records(service_region, config_table_name, config_table_primary_key, db_config_id, sf_config_id, event)
    except Exception as ex:
        exception_message = ("error trying to process AWS JoinExpedia_SFDC_Sync_Inserts\n\n" + 
            "event data passed into job:\n" + json.dumps(event) + "\n\n" + 
            "exception:\n" + str(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)))

        sqs_object = {
            'exceptionMessage': exception_message,
            'event': event
        }
        sqs_string = json.dumps(sqs_object)

        send_to_sqs(sqs_string)

        exception_email = {
            "targetObjectId": os.environ["tech_issue_user"],
            "subject": "Exception encountered in JoinExpedia AWS Job",
            "saveAsActivity": False,
            "plainTextBody": exception_message
        }

        sfdc_login_response = sfdc_helper.get_salesforce_token(service_region, config_table_name, config_table_primary_key, sf_config_id)
        sfdc_access_token = sfdc_login_response.get('access_token')
        sfdc_instance_url = sfdc_login_response.get('instance_url')

        email_result = send_email(sfdc_access_token, sfdc_instance_url, exception_email)
        print("exception: {}".format(exception_message))

        return generate_result(status_code=500, result_message=exception_message)


def create_records(service_region, config_table_name, config_table_primary_key, db_config_id, sf_config_id, join_data):
    """
    This updates or inserts an account, then returns a result with the account 
    id and a flag indicating whether or not this was a duplicate request.

    Args:
        service_region (str): The AWS Service Region. e.g. us-west-2
        config_table_name (str): Name of configuration table storing the 
            encrypted configs
        config_table_primary_key (str): Name of primary key for the 
            configuration table
        db_config_id (str): The primary key for the record you want the 
            configuration details for. In this case this is the configuration 
            for the MySQL credentials.
        sf_config_id (str): The primary key for the record you want the 
            configuration details for. In this case this is the configuration
            for the Salesforce credentials
        join_data (dict): The data being passed from the join form as well as
            the jel mapping data from the previous step.

    Returns:
        dict: Returns a dict with the account id in "sfAccountID" and whether or not
            this is a dupe in "isDupe".
    """
    structure_types = {
        'AGRITOURISM': 'Agritourism',
        'ALL_INCLUSIVE': 'All-inclusive',
        'APART_HOTEL': 'Apart-Hotel',
        'APARTMENT': 'Apartment',
        'BED_BREAKFAST': 'Bed & Breakfast',
        'CABIN': 'Cabin',
        'CARAVAN_PARK': 'Caravan Park',
        'CASTLE': 'Castle',
        'CHALET': 'Chalet',
        'CONDOMINIUM_RESORT': 'Condominium resort',
        'CONDO': 'Condo',
        'COTTAGE': 'Cottage',
        'COUNTRY_HOUSE': 'Country House',
        'CRUISE': 'Cruise',
        'ECO_HOTEL': 'Eco Hotel',
        'GUEST_HOUSE': 'Guest House',
        'HOLIDAY_PARK': 'Holiday Park',
        'HOSTAL_BUDGET': 'Hostal (Budget hotel)',
        'HOSTEL': 'Hostel/Backpacker accommodation',
        'HOTEL_RESORT': 'Hotel resort',
        'HOTEL': 'Hotel',
        'HOUSE_BOAT': 'House boat',
        'INN': 'Inn',
        'LODGE': 'Lodge',
        'LONGHOUSE': 'Longhouse',
        'MOTEL': 'Motel',
        'OVERWATER_ACCOMMODATION': 'Overwater Accommodation',
        'PALACE': 'Palace',
        'PENSION': 'Pension',
        'POUSADA_BRAZIL': 'Pousada (brazil)',
        'PUOSADA_PORTUGAL': 'Pousada (Portugal)',
        'PRIVATE_VACATION_HOME': 'Private Vacation Home',
        'RANCH': 'Ranch',
        'RESIDENCE': 'Residence',
        'RIAD': 'RIAD',
        'RYOKAN': 'Ryokan',
        'SAFARI_TENTALOW': 'Safari / Tentalow',
        'TOWNHOUSE': 'TownHouse',
        'TREE_HOUSE': 'Tree house',
        'VILLA': 'Villa'
    }

    sql_db_connection = database_helper.get_rdb_client(service_region, config_table_name, config_table_primary_key, db_config_id)
    db_cursor = sql_db_connection.cursor(pymysql.cursors.DictCursor)

    dupe_found = False
    account_id = ''

    company_name = join_data.get('companyName','')
    property_name = join_data.get('propertyName','')
    structure_type = join_data.get('structureType','')
    property_type = structure_types.get(structure_type) if structure_type and structure_types.get(structure_type) else structure_type
    number_of_rooms = join_data.get('numberOfRooms')
    number_of_properties = join_data.get('numberOfProperties')
    website = join_data.get('website','')
    address_street = join_data.get('addressStreet','')
    address_city = join_data.get('addressCity','')
    address_postal_code = join_data.get('addressPostalCode','')
    address_country_code = join_data.get('addressCountryCode')
    address_state_province = join_data.get('addressStateProvince','') if (address_country_code == 'United States' or address_country_code == 'USA') else ''
    star_rating = join_data.get('starRating','')
    rating_source = join_data.get('ratingSource','')
    contact_salutation = join_data.get('contactSalutation','')
    contact_first_name = join_data.get('contactFirstName','')
    contact_last_name = join_data.get('contactLastName','')
    contact_email_address = join_data.get('contactEmailAddress','')
    join_phone = join_data.get('contactPhone','')
    contact_role = join_data.get('contactRole','')
    contact_title = join_data.get('contactTitle','')
    fax = join_data.get('fax','')
    language = join_data.get('language','')
    description = join_data.get('description','')
    latitude = join_data.get('latitude')
    longitude = join_data.get('longitude')
    submarket_id = join_data.get('submarketId','')
    client_id = join_data.get('clientId','')
    channel_grouping = join_data.get('channelGrouping','')
    sf_submarket_id = join_data.get('sfSubmarketId') if join_data.get('sfSubmarketId') is not None else None
    record_type_id = '0121A000000GUeh' if number_of_properties is not None and number_of_properties > 1 else '012C0000000gxvS'

    jel_mapping = join_data.get("jelMapping") if join_data.get("jelMapping") else {}

    jel_guideline = ''
    jel_epco_unmapped = False
    jel_action_required = ''
    if submarket_id == "0":
        jel_guideline = 'Add submarket & check contract term (unmapped lead)'
        jel_epco_unmapped = True
        jel_action_required = 'Action Required'

    has_channel_manager = None
    if 'hasChannelManager' in join_data:
        has_channel_manager = 'Yes' if join_data.get('hasChannelManager') else 'No'

    part_of_chain = None
    if 'partOfChain' in join_data:
        part_of_chain = 'Yes' if join_data.get('partOfChain') else 'No'

    account_description = 'Property Name: ' + property_name + '\nProperty Description: ' + description
    time_now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.000Z")

    dupe_query = """
        SELECT Id, AccountId, ContactId 
        FROM join_case 
        WHERE AccountId IS NOT NULL AND 
            Join_Last_Name__c <=> '{1}' AND 
            Join_Phone__c <=> '{2}' AND 
            Join_DBA_Name_Doing_Business_As__c <=> '{3}' AND 
            Join_Street_Address__c <=> '{4}' AND 
            CreatedDate BETWEEN NOW() - INTERVAL 30 DAY AND NOW()
        LIMIT 1
    """.format(join_data["contactLastName"], 
        contact_last_name.replace("'","''"),
        join_phone.replace("'","''"),
        property_name.replace("'","''"),
        address_street.replace("'","''")
    )

    print("\ndupe_query: {}\n\n".format(dupe_query))

    db_cursor.execute(dupe_query)
    dupe_result = db_cursor.fetchall()

    if len(dupe_result) > 0:
        dupe_found = True
        dupe_result = dupe_result[0]
        account_id = dupe_result['AccountId']
    else:
        dupe_result = {}

    billing_country = ''
    if address_country_code:
        country_query = "SELECT * FROM country WHERE ISO_3_country_code__c = '{}'".format(address_country_code)
        db_cursor.execute(country_query)
        country_result = db_cursor.fetchall()

        if len(country_result) > 0 and country_result[0].get("ISO_3_country_code__c") == address_country_code:
            billing_country = country_result[0].get("Name")

    language_id = None
    if language:
        language_query = "SELECT * FROM language WHERE Name = '{}'".format(language)
        db_cursor.execute(language_query)
        language_result = db_cursor.fetchall()

        if len(language_result) > 0:
            language_id = language_result[0].get("Id")

    sfdc_account = {
        'Website': website,
        'BillingStreet': address_street,
        'BillingCity': address_city,
        'BillingCountry': billing_country,
        'BillingPostalCode': address_postal_code[:20],
        'Number_of_Rooms__c': number_of_rooms,
        'BillingState': address_state_province,
        'Phone': join_phone,
        'Fax': fax,
        'Description': account_description,
        'Latitude__c': str(latitude),
        'Longitude__c': str(longitude),
        'Geolocation__longitude__s': longitude,
        'Geolocation__latitude__s': latitude,
        'Joinexpedia_com__c': 'Contact Me',
        'PropertyType__c': property_type,
        'Join_Client_Id__c': client_id,
        'Join_Channel_Grouping__c': channel_grouping,
        'JEL_EPC_Language__c': language,
        'Account_Language__c': language_id,
        'JEL_Has_Channel_Manager__c': has_channel_manager,
        'JEL_Part_of_Chain__c': part_of_chain,
        'Number_of_Properties__c': number_of_properties,
        'JEL_Guidelines__c': jel_guideline,
        'JEL_EPCO_Unmapped__c': jel_epco_unmapped,
        'JEL_action_required__c': jel_action_required
    }

    sfdc_login_response = sfdc_helper.get_salesforce_token(service_region, config_table_name, config_table_primary_key, sf_config_id)
    sfdc_access_token = sfdc_login_response.get('access_token')
    sfdc_instance_url = sfdc_login_response.get('instance_url')

    sfdc_upsert_results = {}
    if dupe_found:
        print("updating existing account.")
        sfdc_upsert_results = pysalesforce.Standard.update_sobject_row('Account', account_id, sfdc_account, sfdc_access_token, sfdc_instance_url)
        
        if sfdc_upsert_results == 'Update Successful':
            sfdc_upsert_results = {'id':account_id,'success': True}
        else:
            sfdc_upsert_results = {'id':account_id,'success': False}
    else:
        print("creating new account.")
        cc_address = None
        if jel_mapping.get('CC_Addresses__c'):
            cc_address = jel_mapping.get('CC_Addresses__c').split(',')[0]

        sfdc_account.update({
            'Name': company_name if number_of_properties is not None and number_of_properties > 1 else property_name,
            'Account_Status__c': 'New - No Action Yet',
            'RecordTypeId': record_type_id,
            'Enrolled__c': 'Yes',
            'Submarket__c': sf_submarket_id,
            'JoinExpedia_Processed_On__c': time_now,
            'Latest_JoinExpedia_Email_Sent_Date__c': time_now,
            'JEL_Mapping__c': jel_mapping.get('Id'),
            'JEL_MMA__c': jel_mapping.get('MMA__c'),
            'JEL_MAA__c': jel_mapping.get('MAA__c'),
            'JEL_Approach_type__c': jel_mapping.get('JEL_Approach_type__c'),
            'JEL_Priority_level__c': jel_mapping.get('JEL_Priority_level__c'),
            'Join_Expedia_Lead_Template_Description__c': jel_mapping.get('Email_Template_Name__c'),
            'Join_Expedia_Lead_Owner__c': jel_mapping.get('Join_Expedia_Lead_Owner__c'),
            'Join_Expedia_Lead_Commercial_Conditions__c': jel_mapping.get('Commercial_Conditions__c'),
            'Join_Expedia_Lead_Template_Type__c': jel_mapping.get('Template_Type__c'),
            'Join_Expedia_Lead_CC_Address__c': cc_address
        })

        print("sfdc_account: {}".format(sfdc_account))
        sfdc_upsert_results = pysalesforce.Standard.create_sobject_row('Account', sfdc_account, sfdc_access_token, sfdc_instance_url)
        print("sfdc_upsert_results: {}".format(sfdc_upsert_results))
        sfdc_upsert_results = json.loads(sfdc_upsert_results)
        print(sfdc_upsert_results)
        account_id = sfdc_upsert_results.get('id')

    sns_data = {
        "joinData": join_data,
        "accountData": sfdc_account,
        "upsertData": sfdc_upsert_results,
        "languageId": language_id,
        "isDupe": dupe_found,
        "dupeData": dupe_result
    }

    send_to_sns(sns_data)

    print("\ndata sent to sns: {}\n\n".format(sns_data))

    if account_id:
        return generate_result(account_id=account_id, is_dupe=dupe_found)

def generate_result(**kwargs):
    result_message = {}
    result_message['statusCode'] = kwargs.get('status_code', 200)
    result_message['headers'] = kwargs.get('headers', {'Content-Type': 'application/json'})
    result_message['isBase64Encoded'] = kwargs.get('isBase64Encoded', False)
    result_body = {}
    result_body['resultMessage'] = kwargs.get('result_message', 'success')

    if 'account_id' in kwargs:
        result_body['sfAccountID'] = kwargs.get('account_id')
    if 'is_dupe' in kwargs:
        result_body['isDupe'] = kwargs.get('is_dupe')

    result_message['body'] = result_body

    return result_message


def send_email(sfdc_access_token, sfdc_instance_url, email_object):
    """
    Sends an email with the custom Salesforce email send web service and 
    returns the response from the service.

    Args:
        sfdc_access_token (str): The access token returned from the 
                                 Salesforce login response.
        sfdc_instance_url (str): the instance url returned from the 
                                 Salesforce login response.
        email_object (dict): The dict containing all the attributes
                             that will be used to send an email with 
                             the custom service.

    Returns:
        dict: Returns a a list object for each email sent. This will only
              be a single item since this sends a single email. The result
              will contain 2 fields: sendIsSuccess to say whether or not
              the email send was successful, and emailErrors which is a 
              string array with any errors encountered when trying to send
              the email.
    """
    email_service_uri = "/services/apexrest/SendEmailService/"
    header_details = pysalesforce.Util.get_standard_header(sfdc_access_token)

    email_object = {
        "emails": [email_object],
        "sendAllOrNothing": False
    }

    json_data_body = json.dumps(email_object, indent=4, separators=(',', ': '))
    response = webservice.Tools.post_http_response(sfdc_instance_url + email_service_uri, json_data_body, header_details)
    print(response.text)
    json_response = json.loads(response.text)

    return json_response

def send_to_sns(sns_message):
    """
    Send this message into the sns

    Args:
        sns_message (str): The message to be pushed into the sns

    Returns
        dict: The response from aws publishing the message
    """
    sns = boto3.client('sns')
    response = sns.publish(
        TopicArn=os.environ["sns_arn"],
        Message=json.dumps(sns_message)
    )
    return response

def send_to_sqs(sqs_message):
    """
    Push this message into the SQS

    Args:
        sqs_message (str): The message to be sent to sqs

    Returns:
        dict: The response from aws after sending the message
    """
    sqs = boto3.client('sqs')
    response = sqs.send_message(
        QueueUrl=os.environ["sqs_url"],
        MessageBody=sqs_message,
        MessageGroupId='1'
    )
    return response

# if __name__ == "__main__":
#     # AWS settings
#     os.environ["service_region"] = 'us-west-2'
#     os.environ["config_table_name"] = 'app_configuration'
#     os.environ["config_table_primary_key"] = 'configuration_name'
#     os.environ['db_config'] = 'jel_mapping_rds_localhost_v2'
#     # os.environ["db_config"] = "jel_mapping_rds_staging_v2"
#     os.environ["sns_arn"] = 'arn:aws:sns:us-west-2:198743346998:join-expedia-async-inserts'
#     os.environ["sqs_url"] = 'https://sqs.us-west-2.amazonaws.com/198743346998/join-expedia-sync-insert-exceptions.fifo'

#     # Salesforce settings
#     os.environ["sf_config"] = 'salesforce_gsosfdc_staging_v2'

#     # contact settings
#     os.environ["join_expedia_contact_id"] = "0030x00000AMuMB"
#     os.environ["missing_jel_template_id"] = "00X0x000000DkrW"
#     os.environ["tech_issue_user"] = "0051A000009GjJDQA0"

#     # test data
#     join_data = {
#         'propertyName': 'test jel api create property',
#         'structureType': 'HOTEL',
#         'numberOfRooms': 5,
#         'numberOfProperties': 1,
#         'website': 'www.example.com',
#         'addressStreet': '9 Marlborough St',
#         'addressCity': 'Boston',
#         'addressPostalCode': '02116',
#         'addressCountryCode': 'USA',
#         'addressStateProvince': 'MA',
#         'starRating': '3.5',
#         'ratingSource': 'AAA',
#         'contactSalutation': 'Mr.',
#         'contactFirstName': 'Glen',
#         'contactLastName': 'Barger',
#         'contactEmailAddress': 'gbarger@gmail.com',
#         'contactPhone': '(123) 456-7890',
#         'contactRole': 'Owner',
#         'contactTitle': 'Owner',
#         'fax': '(123) 456-7890',
#         'language': 'English',
#         'description': 'new account description.',
#         'latitude': 42.354601,
#         'longitude': -71.072998,
#         'submarketId': '123534',
#         'clientId': '1234.5678',
#         'channelGrouping': 'channel grouping',
#         'hasChannelManager': False,
#         'partOfChain': False,`
#         'sfSubmarketId': 'a0DC000000X6mvI',
#         'jelMapping': '{"Id": "a2E1A000001KF0WUAW", "Name": "JEM-25117", "Active__c": 1, "CC_Addresses__c": "naregional@expedia.com", "Email_Template_Id__c": "00X1A00000287GC", "Market__c": "a09C000000BfYeUIAV", "MMA__c": null, "MAA__c": null, "Room_Maximum__c": 10, "Room_Minimum__c": 0, "Property_Minimum__c": null, "Property_Maximum__c": null, "Rule_Description__c": null, "From_Org_Wide_Email_Id__c": "0D21A00000001aiSAA", "Submarket__c": null, "Commercial_Conditions__c": "Not targeted", "Do_Not_CC_JE_Lead_Owner__c": 1, "Email_Template_Name__c": "AMER: Not targeted (<10 rooms, Hawaii)", "From_Org_Wide_Email_Address__c": "joinexpediausa@expedia.com", "Join_Country_ISO_3_Code__c": "USA", "Join_Country_Lookup__c": "a0BC000000FASOLMA5", "Join_Expedia_Lead_Owner__c": "005C0000007Iy3EIAS", "Join_Expedia_Lead_Owner_Name__c": "Adrian Boettcher", "Lead_Owner_Email__c": "aboettcher=expedia.com@example.com", "Region__c": "a0AC000000ICLonMAH", "Template_Type__c": "Not targeted", "JEL_Approach_type__c": null, "JEL_Offer_Base_allocation__c": null, "JEL_Offer_Business_model__c": null, "JEL_Offer_Compensation__c": null, "JEL_Offer_Fenced_discount__c": null, "JEL_Priority_level__c": null, "RecordTypeDeveloperName": "Hotel_Mapping"}'
#     }

#     get_results = lambda_handler(join_data, '')
#     print('get_results: {}'.format(get_results))

#     # sfdc_login_response = sfdc_helper.get_salesforce_token('us-west-2', 'app_configuration', 'configuration_name', 'salesforce_gsosfdc_staging_v2')
#     # sfdc_access_token = sfdc_login_response.get('access_token')
#     # sfdc_instance_url = sfdc_login_response.get('instance_url')

#     # email_object = {
#     #     "toAddresses": ["gbarger@gmail.com"],
#     #     "subject": "test email send from python through salesforce",
#     #     "plainTextBody": "plain text body from test custom webservice email send."
#     # }

#     # email_result = send_email(sfdc_access_token, sfdc_instance_url, email_object)
#     # print("email_result: {}".format(email_result))