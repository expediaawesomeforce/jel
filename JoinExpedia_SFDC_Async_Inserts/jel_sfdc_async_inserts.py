import os
import sys
sys.path.append("{}/pip_libs".format(os.path.dirname(os.path.realpath(__file__))))
import traceback
import json
import ast
import time
from datetime import datetime

import pymysql
import boto3

from config import Config
import sfdc_helper
import database_helper
import webservice
import pysalesforce


def lambda_handler(event, context):
    print(event)
    print(type(event))
    if 'Records' in event:
        message_body = event.get('Records')[0].get('Sns').get('Message')
        print(type(message_body))
        event = json.loads(message_body)
        if type(event) == str:
            print('unicode')
            message_body = ast.literal_eval(message_body)
        event = json.loads(message_body)

    print(event)

    service_region = os.environ["service_region"]
    config_table_name = os.environ["config_table_name"]
    config_table_primary_key = os.environ["config_table_primary_key"]
    db_config_id = os.environ['db_config']
    sf_config_id = os.environ["sf_config"]

    try:
        return create_records(service_region, config_table_name, config_table_primary_key, db_config_id, sf_config_id, event)
    except Exception as ex:    
        exception_message = ("error trying to process AWS JoinExpedia_SFDC_Async_Inserts\n\n" + 
            "event data passed into job:\n" + json.dumps(event) + "\n\n" + 
            "exception:\n" + str(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)))

        sqs_object = {
            'exceptionMessage': exception_message,
            'event': event
        }

        sqs_string = json.dumps(sqs_object)
        send_to_sqs(sqs_string)

        exception_email = {
            "targetObjectId": os.environ["tech_issue_user"],
            "subject": "Exception encountered in JoinExpedia AWS Job",
            "saveAsActivity": False,
            "plainTextBody": exception_message
        }

        sfdc_login_response = sfdc_helper.get_salesforce_token(service_region, config_table_name, config_table_primary_key, sf_config_id)
        sfdc_access_token = sfdc_login_response.get('access_token')
        sfdc_instance_url = sfdc_login_response.get('instance_url')

        email_result = send_email(sfdc_access_token, sfdc_instance_url, exception_email)
        print("exception: {}".format(exception_message))

        return generate_result(status_code=500, result_message=exception_message)


def create_records(service_region, config_table_name, config_table_primary_key, db_config_id, sf_config_id, join_data):
    """
    This this is not a dupe jel request, this creates a new contact to be attached to the account and case, then 
    it inserts the case and also updates the account to link it to the join case.

    Args:
        service_region (str): The AWS Service Region. e.g. us-west-2
        config_table_name (str): Name of configuration table storing the 
            encrypted configs
        config_table_primary_key (str): Name of primary key for the 
            configuration table
        db_config_id (str): The primary key for the record you want the 
            configuration details for. In this case this is the configuration 
            for the MySQL credentials.
        sf_config_id (str): The primary key for the record you want the 
            configuration details for. In this case this is the configuration
            for the Salesforce credentials
        join_data (dict): The data being passed from the join form as well as
            other data from the sync step to save time in getting that data 
            again.
            attributes in the object are:
            joinData: The join data received by the previous step
            accountData: The account object that was inserted or updated in 
                the previous step
            upsertData: The result from inserting or updating the account 
                in the previous step
            languageId: The Salesforce record Id for the language that 
                was set on the account and also needs to be set on the contact
            isDupe: This is a boolean that displays whether or not the account
                id returned in the previous step is a dupe based on the jel
                information sent and finding a similar jel in the last 30 days.
            dupeData: If a dupe was found in the query in the previous step, 
                it will be stored here.

    Returns:
        dict: Returns 200 success if successful.
    """
    sfdc_login_response = sfdc_helper.get_salesforce_token(service_region, config_table_name, config_table_primary_key, sf_config_id)
    sfdc_access_token = sfdc_login_response.get('access_token')
    sfdc_instance_url = sfdc_login_response.get('instance_url')

    join_case = {
        'AccountId': join_data.get('upsertData').get('id') if join_data.get('upsertData') else None,
        'Join_DBA_Name_Doing_Business_As__c': join_data.get('joinData').get('propertyName') if join_data.get('joinData') else None,
        'Join_Structure_Type__c': join_data.get('accountData').get('PropertyType__c') if join_data.get('accountData') else None,
        'Join_Number_of_Rooms__c': join_data.get('joinData').get('numberOfRooms') if join_data.get('joinData') else None,
        'Join_Number_of_Properties__c': join_data.get('joinData').get('numberOfProperties') if join_data.get('joinData') else None,
        'Join_Website__c': join_data.get('joinData').get('website') if join_data.get('joinData') else None,
        'Join_Street_Address__c': join_data.get('joinData').get('addressStreet') if join_data.get('joinData') else None,
        'Join_City__c': join_data.get('joinData').get('addressCity') if join_data.get('joinData') else None,
        'Join_Postal_Code_Zip__c': join_data.get('joinData').get('addressPostalCode') if join_data.get('joinData') else None,
        'Join_Country__c': join_data.get('joinData').get('addressCountryCode') if join_data.get('joinData') else None,
        'Join_Region__c': join_data.get('joinData').get('addressStateProvince') if join_data.get('joinData') else None,
        'Join_Star_Rating__c': join_data.get('joinData').get('starRating') if join_data.get('joinData') else None,
        'Join_Rating_Source__c': join_data.get('joinData').get('ratingSource') if join_data.get('joinData') else None,
        'Join_Salutation__c': join_data.get('joinData').get('contactSalutation') if join_data.get('joinData') else None,
        'Join_First_Name__c': join_data.get('joinData').get('contactFirstName') if join_data.get('joinData') else None,
        'Join_Last_Name__c': join_data.get('joinData').get('contactLastName') if join_data.get('joinData') else None,
        'Join_Email__c': join_data.get('joinData').get('contactEmailAddress') if join_data.get('joinData') else None,
        'Join_Phone__c': join_data.get('joinData').get('contactPhone') if join_data.get('joinData') else None,
        'Join_Role__c': join_data.get('joinData').get('contactRole') if join_data.get('joinData') else None,
        'Join_Title__c': join_data.get('joinData').get('contactTitle') if join_data.get('joinData') else None,
        'Join_Fax__c': join_data.get('joinData').get('fax') if join_data.get('joinData') else None,
        'Description': join_data.get('joinData').get('description') if join_data.get('joinData') else None,
        'RecordTypeId': "012C0000000gxve",
        'Join_Lat__c': join_data.get('joinData').get('latitude') if join_data.get('joinData') else None,
        'Join_Lon__c': join_data.get('joinData').get('longitude') if join_data.get('joinData') else None,
        'Join_Submarket_Id__c': join_data.get('joinData').get('submarketId') if join_data.get('joinData') else None,
        'Join_Client_Id__c': join_data.get('joinData').get('clientId') if join_data.get('joinData') else None,
        'Join_Channel_Grouping__c': join_data.get('joinData').get('channelGrouping') if join_data.get('joinData') else None,
        'JEL_EPC_Language__c': join_data.get('joinData').get('language') if join_data.get('joinData') else None
    }

    if 'isDupe' in join_data and not join_data.get('isDupe'):
        # create the contact
        join_contact = {
            'LastName': join_case.get('Join_Last_Name__c'),
            'FirstName': join_case.get('Join_First_Name__c'),
            'Salutation': join_case.get('Join_Salutation__c'),
            'Title': join_case.get('Join_Title__c'),
            'Email': join_case.get('Join_Email__c'),
            'Phone': join_case.get('Join_Phone__c'),
            'Fax': join_case.get('Join_Fax__c'),
            'Role__c': join_case.get('Join_Role__c'),
            'Primary_Contact__c': True,
            'Language__c': join_data.get('joinData').get('language') if join_data.get('joinData') else None,
            'AccountId': join_case.get('AccountId')
        }

        contact_create_results = pysalesforce.Standard.create_sobject_row('Contact', join_contact, sfdc_access_token, sfdc_instance_url)
        contact_create_results = json.loads(contact_create_results)

        # create the case
        join_case.update({
            'ContactId': contact_create_results.get('id')
        })

        case_create_results = pysalesforce.Standard.create_sobject_row('Case', join_case, sfdc_access_token, sfdc_instance_url)
        case_create_results = json.loads(case_create_results)
        print(case_create_results)

        # update the account with the join case Id
        account_update = {
            'Id': join_data.get('upsertData').get('id'),
            'Join_Expedia_Lead_Case__c': case_create_results.get('id')
        }

        sfdc_upsert_results = pysalesforce.Standard.update_sobject_row('Account', join_data.get('upsertData').get('id'), account_update, sfdc_access_token, sfdc_instance_url)
        sfdc_upsert_results = json.loads(sfdc_upsert_results)

    elif 'isDupe' in join_data and join_data.get('isDupe'):
        join_case.update({
            'Status': 'Closed',
            'ContactId': join_data.get('dupeData').get('ContactId')
        })

        case_create_results = pysalesforce.Standard.create_sobject_row('Case', join_case, sfdc_access_token, sfdc_instance_url)
        case_create_results = json.loads(case_create_results)

    join_case.update({
        'Id': case_create_results.get('id')
    })

    # insert the join case to the db
    join_case_configuration = {
        'sfdc_object_name': 'Case',
        'sql_table_name': 'join_case',
        'where_clause': 'RecordType.DeveloperName = \'JoinLeads\' AND CreatedDate = LAST_N_DAYS:30',
        'fields': [
            {'sfdc_field_name': 'Id', 'sql_field_name': 'Id', 'sql_field_properties': 'VARCHAR(18) NOT NULL UNIQUE', 'sf_data_type': 'id'},
            {'sfdc_field_name': 'CreatedDate', 'sql_field_name': 'CreatedDate', 'sql_field_properties': 'DATETIME(3)', 'sf_data_type': 'datetime'},
            {'sfdc_field_name': 'ContactId', 'sql_field_name': 'ContactId', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
            {'sfdc_field_name': 'AccountId', 'sql_field_name': 'AccountId', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
            {'sfdc_field_name': 'Join_Last_Name__c', 'sql_field_name': 'Join_Last_Name__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
            {'sfdc_field_name': 'Join_Phone__c', 'sql_field_name': 'Join_Phone__c', 'sql_field_properties': 'VARCHAR(40)', 'sf_data_type': 'string'},
            {'sfdc_field_name': 'Join_DBA_Name_Doing_Business_As__c', 'sql_field_name': 'Join_DBA_Name_Doing_Business_As__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
            {'sfdc_field_name': 'Join_Street_Address__c', 'sql_field_name': 'Join_Street_Address__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'}
        ]
    }

    # generate timestamp for CreatedDate
    time_now = time.time()
    created_date = datetime.fromtimestamp(time_now).strftime('%Y-%m-%d %H:%M:%S')

    # get the join case upsert string
    case_upsert_string = """
        INSERT INTO JoinExpediaConfigurationMapping.join_case
            (Id, CreatedDate, ContactId, AccountId, 
            Join_Last_Name__c, Join_Phone__c, 
            Join_DBA_Name_Doing_Business_As__c, Join_Street_Address__c)
        VALUES
            {0}
        ON DUPLICATE KEY UPDATE
            Id = VALUES(Id),
            CreatedDate = VALUES(CreatedDate),
            ContactId = VALUES(ContactId),
            AccountId = VALUES(AccountId),
            Join_Last_Name__c = VALUES(Join_Last_Name__c),
            Join_Phone__c = VALUES(Join_Phone__c),
            Join_DBA_Name_Doing_Business_As__c = VALUES(Join_DBA_Name_Doing_Business_As__c),
            Join_Street_Address__c = VALUES(Join_Street_Address__c)
    """

    # create the case value for the upsert string
    case_value_list = [
        "'" + join_case.get('Id') + "'",
        "'" + created_date + "'",
        "'" + join_case.get('ContactId').replace("'","''") + "'" if join_case.get('ContactId') else 'NULL',
        "'" + join_case.get('AccountId').replace("'","''") + "'" if join_case.get('AccountId') else 'NULL',
        "'" + join_case.get('Join_Last_Name__c').replace("'","''") + "'" if join_case.get('Join_Last_Name__c') else 'NULL',
        "'" + join_case.get('Join_Phone__c').replace("'","''") + "'" if join_case.get('Join_Phone__c') else 'NULL',
        "'" + join_case.get('Join_DBA_Name_Doing_Business_As__c').replace("'","''") + "'" if join_case.get('Join_DBA_Name_Doing_Business_As__c') else 'NULL',
        "'" + join_case.get('Join_Street_Address__c').replace("'","''") + "'" if join_case.get('Join_Street_Address__c') else 'NULL'
    ]

    # place the values for the upsert into the upsert string and execute the db action
    case_value_string = '(' + ",".join(case_value_list) + ')'
    case_upsert_string = case_upsert_string.format(case_value_string)

    db_client = database_helper.get_rdb_client(service_region, config_table_name, config_table_primary_key, db_config_id)
    db_cursor = db_client.cursor(pymysql.cursors.DictCursor)
    db_cursor.execute(case_upsert_string)

    db_client.commit()
    db_client.close()


    # send the emails
    if not join_data.get('joinData').get('jelMapping'):
        missing_map_email = {
            'targetObjectId': os.environ['join_expedia_contact_id'],
            'whatId': join_case.get('AccountId'),
            'templateId': os.environ['missing_jel_template_id'],
            'saveAsActivity': False
        }

        email_result = send_email(sfdc_access_token, sfdc_instance_url, missing_map_email)
        print("unmapped email_result: {}".format(email_result))
    elif join_data.get('joinData').get('jelMapping') and join_data.get('joinData').get('jelMapping').get('JEL_Approach_type__c') != 'Self-service' and join_data.get('joinData').get('jelMapping').get('Email_Template_Id__c'):
        print('Sending email')
        jel_email = {
            'templateId': join_data.get('joinData').get('jelMapping').get('Email_Template_Id__c'),
            'targetObjectId': join_case.get('ContactId'),
            'whatId': join_case.get('AccountId')
        }

        if join_data.get('joinData').get('jelMapping').get('From_Org_Wide_Email_Id__c'):
            jel_email['orgWideEmailAddressId'] = join_data.get('joinData').get('jelMapping').get('From_Org_Wide_Email_Id__c')

        email_result = send_email(sfdc_access_token, sfdc_instance_url, jel_email)
        print("jel email_result: {}".format(email_result))

    return generate_result()

def generate_result(**kwargs):
    result_message = {}
    result_message['statusCode'] = kwargs.get('status_code', 200)
    result_message['headers'] = kwargs.get('headers', None)

    result_body = {}
    result_body['resultMessage'] = kwargs.get('result_message', 'success')    

    result_message['body'] = json.dumps(result_body)

    return result_message


def send_email(sfdc_access_token, sfdc_instance_url, email_object):
    """
    Sends an email with the custom Salesforce email send web service and 
    returns the response from the service.

    Args:
        sfdc_access_token (str): The access token returned from the 
                                 Salesforce login response.
        sfdc_instance_url (str): the instance url returned from the 
                                 Salesforce login response.
        email_object (dict): The dict containing all the attributes
                             that will be used to send an email with 
                             the custom service.

    Returns:
        dict: Returns a a list object for each email sent. This will only
              be a single item since this sends a single email. The result
              will contain 2 fields: sendIsSuccess to say whether or not
              the email send was successful, and emailErrors which is a 
              string array with any errors encountered when trying to send
              the email.
    """
    email_service_uri = "/services/apexrest/SendEmailService/"
    header_details = pysalesforce.Util.get_standard_header(sfdc_access_token)

    email_object = {
        "emails": [email_object],
        "sendAllOrNothing": False
    }

    json_data_body = json.dumps(email_object, indent=4, separators=(',', ': '))
    response = webservice.Tools.post_http_response(sfdc_instance_url + email_service_uri, json_data_body, header_details)
    json_response = json.loads(response.text)

    return json_response


def send_to_sqs(sqs_message):
    """
    Push this message into the SQS

    Args:
        sqs_message (str): The message to be sent to sqs

    Returns:
        dict: The response from aws after sending the message
    """
    sqs = boto3.client('sqs')
    response = sqs.send_message(
        QueueUrl=os.environ["sqs_url"],
        MessageBody=sqs_message,
        MessageGroupId='1'
    )
    return response


# if __name__ == "__main__":
#     # AWS settings
#     os.environ["service_region"] = 'us-west-2'
#     os.environ["config_table_name"] = 'app_configuration'
#     os.environ["config_table_primary_key"] = 'configuration_name'
#     os.environ['db_config'] = 'jel_mapping_rds_localhost_v2'
#     # os.environ["db_config"] = "jel_mapping_rds_staging_v2"
#     os.environ["sns_arn"] = 'arn:aws:sns:us-west-2:198743346998:join-expedia-async-inserts'
#     os.environ["sqs_url"] = 'https://sqs.us-west-2.amazonaws.com/198743346998/join-expedia-async-insert-exceptions.fifo'

#     # Salesforce settings
#     os.environ["sf_config"] = 'salesforce_gsosfdc_staging_v2'
#     os.environ["tech_issue_user"] = "0051A000009GjJDQA0"

#     # contact settings
#     os.environ["join_expedia_contact_id"] = "0031g000005za0y"
#     os.environ["missing_jel_template_id"] = "00X1g000000QHHY"

#     join_data = {'joinData': {'propertyName': 'test jel api create property', 'structureType': 'HOTEL', 'numberOfRooms': 5, 'numberOfProperties': 1, 'website': 'www.example.com', 'addressStreet': '9 Marlborough St', 'addressCity': 'Boston', 'addressPostalCode': '02116', 'addressCountryCode': 'USA', 'addressStateProvince': 'MA', 'starRating': '3.5', 'ratingSource': 'AAA', 'contactSalutation': 'Mr.', 'contactFirstName': 'Glen', 'contactLastName': 'Barger', 'contactEmailAddress': 'gbarger@gmail.com', 'contactPhone': '(123) 456-7890', 'contactRole': 'Owner', 'contactTitle': 'Owner', 'fax': '(123) 456-7890', 'language': 'English', 'description': 'new account description.', 'latitude': 42.354601, 'longitude': -71.072998, 'submarketId': '123534', 'clientId': '1234.5678', 'channelGrouping': 'channel grouping', 'hasChannelManager': False, 'partOfChain': False, 'sfSubmarketId': 'a0DC000000X6mvI', 'jelMapping': '{"Id": "a2E1A000001KF0WUAW", "Name": "JEM-25117", "Active__c": 1, "CC_Addresses__c": "naregional@expedia.com", "Email_Template_Id__c": "00X1A00000287GC", "Market__c": "a09C000000BfYeUIAV", "MMA__c": null, "MAA__c": null, "Room_Maximum__c": 10, "Room_Minimum__c": 0, "Property_Minimum__c": null, "Property_Maximum__c": null, "Rule_Description__c": null, "From_Org_Wide_Email_Id__c": "0D21A00000001aiSAA", "Submarket__c": null, "Commercial_Conditions__c": "Not targeted", "Do_Not_CC_JE_Lead_Owner__c": 1, "Email_Template_Name__c": "AMER: Not targeted (<10 rooms, Hawaii)", "From_Org_Wide_Email_Address__c": "joinexpediausa@expedia.com", "Join_Country_ISO_3_Code__c": "USA", "Join_Country_Lookup__c": "a0BC000000FASOLMA5", "Join_Expedia_Lead_Owner__c": "005C0000007Iy3EIAS", "Join_Expedia_Lead_Owner_Name__c": "Adrian Boettcher", "Lead_Owner_Email__c": "aboettcher=expedia.com@example.com", "Region__c": "a0AC000000ICLonMAH", "Template_Type__c": "Not targeted", "JEL_Approach_type__c": null, "JEL_Offer_Base_allocation__c": null, "JEL_Offer_Business_model__c": null, "JEL_Offer_Compensation__c": null, "JEL_Offer_Fenced_discount__c": null, "JEL_Priority_level__c": null, "RecordTypeDeveloperName": "Hotel_Mapping"}'}, 'accountData': {'Website': 'www.example.com', 'BillingStreet': '9 Marlborough St', 'BillingCity': 'Boston', 'BillingCountry': 'United States', 'BillingPostalCode': '02116', 'Number_of_Rooms__c': 5, 'BillingState': 'MA', 'Phone': '(123) 456-7890', 'Fax': '(123) 456-7890', 'Description': 'Property Name: test jel api create property\nProperty Description: new account description.', 'Latitude__c': '42.354601', 'Longitude__c': '-71.072998', 'Geolocation__longitude__s': -71.072998, 'Geolocation__latitude__s': 42.354601, 'Joinexpedia_com__c': 'Contact Me', 'PropertyType__c': 'Hotel', 'Join_Client_Id__c': '1234.5678', 'Join_Channel_Grouping__c': 'channel grouping', 'JEL_EPC_Language__c': 'English', 'Account_Language__c': 'a0VC00000039gusMAA', 'JEL_Has_Channel_Manager__c': 'No', 'JEL_Part_of_Chain__c': 'No', 'Number_of_Properties__c': 1, 'Name': 'test jel api create property', 'Account_Status__c': 'New - No Action Yet', 'RecordTypeId': '012C0000000gxvS', 'Enrolled__c': 'Yes', 'Submarket__c': 'a0DC000000X6mvI', 'JoinExpedia_Processed_On__c': '2018-03-29T19:00:00.000Z', 'Latest_JoinExpedia_Email_Sent_Date__c': '2018-03-29T19:00:00.000Z', 'JEL_Mapping__c': 'a2E1A000001KF0WUAW', 'JEL_MMA__c': None, 'JEL_MAA__c': None, 'JEL_Approach_type__c': None, 'JEL_Priority_level__c': None, 'Join_Expedia_Lead_Template_Description__c': 'AMER: Not targeted (<10 rooms, Hawaii)', 'Join_Expedia_Lead_Owner__c': '005C0000007Iy3EIAS', 'Join_Expedia_Lead_Commercial_Conditions__c': 'Not targeted', 'Join_Expedia_Lead_Template_Type__c': 'Not targeted', 'Join_Expedia_Lead_CC_Address__c': 'naregional@expedia.com'}, 'upsertData': {'id': '0011g000007IVwMAAW', 'success': True, 'errors': []}, 'languageId': 'a0VC00000039gusMAA', 'isDupe': False, 'dupeData': {}}
#     lambda_handler(join_data, None)