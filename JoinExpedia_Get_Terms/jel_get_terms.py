import os
import sys
sys.path.append("{}/pip_libs".format(os.path.dirname(os.path.realpath(__file__))))
import traceback
from datetime import datetime, timedelta

import pymysql

from config import Config
import database_helper
import json

time_store = datetime.now()
def lambda_handler(event, context):
    """
    This handles the input coming from Lambda and starts the get_jel_terms 
    with the os.environ settings and the event details then returns the 
    correct jel terms or an error if no result is possible.

    Args:
        event (obj): This is the join expedia data. The current fields are:
            numberOfRooms: the number of hotel rooms. This is required if 
                numberOfProperties is blank.
            numberOfProperties: the number of VR properties. This is required 
                if rooms is blank.
            submarketId: the submarketId this hotel is located in. This will 
                be used to find the market, region, and  super region
            addressCountryCode: This is the 3 letter ISO country code the 
                hotel is located. e.g. USA or GBR

    Returns:
        dict: This returns a dict with the following values:
            status_code: representing the HTTP status
            body: that will be the body of the messgae returned by the 
                service. The body may contain several fields as well.
                result_message: this will always be available. If the request
                    was successful, this will just be "success", but if there 
                    was an error processing the request including any internal
                    errors, the error will be displayed here.
                jel_offer_business_model: this is the business model returned
                    from the jel mapping
                jel_offer_pct_compensation: this is the pct compensation
                    returned from the jel mapping
                jel_offer_pct_fenced_discount: this is the pct fenced 
                    discount returned from the jel mapping
                jel_offer_pct_base_allocation: this is the pct base 
                    allocation returned from the jel mapping
                jel_approach_type: this is the approach type returned from
                    the jel mapping
                jel_priority_level: this is the priority level returned from
                    the jel mapping
                jel_expedia_lead_owner: this is the jel owner returned from 
                    the jel mapping
                lead_owner_email: this is the lead owner email returned from
                    the jel mapping
                jel_mapping: this is all record details returned from the 
                    jel mapping object to be handed over to the jel create 
                    api.
    """
    if 'detail-type' in event and event.get('detail-type') == 'Scheduled Event':
        print(event.get('detail-type'))
        return

    if 'body' in event:
        event = json.loads(event['body'])

    service_region = os.environ["service_region"]
    config_table_name = os.environ["config_table_name"]
    config_table_primary_key = os.environ["config_table_primary_key"]
    db_config = os.environ['db_config']
    join_data = event

    try:
        return get_jel_terms(service_region, config_table_name, config_table_primary_key, db_config, join_data)
    except Exception as ex:
        exception_message = "".join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        error_message = str({"request":event, "exception":exception_message})
        return generate_result(result_message=error_message, status_code=500)


def get_jel_terms(service_region, config_table_name, config_table_primary_key, db_config_id, join_data):
    """
    Searches for the appropriate join expedia lead email mapping record and
    returns the appropriate mapping based on the input details in the join_data

    Args:
        service_region (str): The AWS Service Region. e.g. us-west-2
        config_table_name (str): Name of configuration table storing the
            encrypted configs
        config_table_primary_key (str): Name of primary key for the
            configuration table
        db_config_id (str): The primary key for the record you want the
            configuration details for. In this case this is the configuration
            for the MySQL credentials.
        join_data (obj): This is the join expedia data. The current fields are:
            numberOfRooms: the number of hotel rooms. This is required if
                numberOfProperties is blank.
            numberOfProperties: the number of VR properties. This is required
                if rooms is blank.
            submarketId: the submarketId this hotel is located in. This will
                be used to find the market, region, and  super region
            addressCountryCode: This is the 3 letter ISO country code the
                hotel is located. e.g. USA or GBR

    Returns:
        dict: See the details for the return in the comments for the
            lambda_handler.
    """
    field_validate_pass, field_validate_message = validate_fields(join_data)

    if not field_validate_pass:
        return generate_result(result_message=field_validate_message, status_code=400)

    if join_data.get('numberOfRooms'):
        if join_data['numberOfRooms'] < 0:
            join_data['numberOfRooms'] = 0
        elif join_data['numberOfRooms'] > 9999:
            join_data['numberOfRooms'] = 9999
    if join_data.get('numberOfProperties'):
        if join_data['numberOfProperties'] < 0:
            join_data['numberOfProperties'] = 0
        if join_data['numberOfProperties'] > 9999:
            join_data['numberOfProperties'] = 9999

    sql_db_connection = database_helper.get_rdb_client(service_region, config_table_name, config_table_primary_key, db_config_id)
    db_cursor = sql_db_connection.cursor(pymysql.cursors.DictCursor)

    submarket_query = "SELECT * FROM submarket WHERE Expedia_Sub_Market_Id__c = '" + join_data['submarketId'] + "'"
    db_cursor.execute(submarket_query)
    submarket_result = db_cursor.fetchall()

    if len(submarket_result) == 0:
        return generate_result(result_message="The provided submarket '{}' could not be found.".format(join_data['submarketId']), status_code=501)

    submarket_result = submarket_result[0]

    jel_mapping_query = "SELECT * FROM join_expedia_lead_email_mapping WHERE Active__c = 1 AND (Submarket__c = '" + submarket_result.get('Id') + "' "

    if submarket_result.get('Market__c'):
        jel_mapping_query += "OR (Submarket__c IS NULL AND Market__c = '" + submarket_result['Market__c'] + "') "
    if submarket_result.get('Region__c'):
        jel_mapping_query += "OR (Submarket__c IS NULL AND Market__c IS NULL AND Region__c = '" + submarket_result['Region__c'] + "') "
    if join_data.get('addressCountryCode'):
        jel_mapping_query += "OR (Submarket__c IS NULL AND Market__c IS NULL AND Region__c IS NULL AND Join_Country_ISO_3_Code__c = '" + join_data['addressCountryCode'] + "') "


    if join_data['numberOfProperties'] and join_data['numberOfProperties'] > 1:
        jel_mapping_query += ') AND ((Property_Maximum__c IS NULL AND Property_Minimum__c IS NULL) OR (Property_Minimum__c <= ' + str(join_data['numberOfProperties']) + ' AND Property_Maximum__c >= ' + str(join_data['numberOfProperties']) + ')) AND RecordTypeDeveloperName = \'VR_Mapping\''
    else:
        jel_mapping_query += ') AND ((Room_Maximum__c IS NULL AND Room_Minimum__c IS NULL) OR (Room_Minimum__c <= ' + str(join_data['numberOfRooms']) + ' AND Room_Maximum__c >= ' + str(join_data['numberOfRooms']) + ')) AND RecordTypeDeveloperName = \'Hotel_Mapping\''

    jel_mapping_query += ' ORDER BY Submarket__c DESC, Market__c DESC, Region__c DESC, Join_Country_ISO_3_Code__c DESC, Room_Maximum__c DESC'

    db_cursor.execute(jel_mapping_query)
    jel_mapping_query_result = db_cursor.fetchall()
    jel_mapping_result_len = len(jel_mapping_query_result)

    if jel_mapping_result_len == 0:
        return generate_result(result_message="There were no join expedia mappings found for the provided geography and properties/rooms.", status_code=501)
    else:
        mapping = jel_mapping_query_result[0]
        return generate_result(
            result_message="success",
            jel_offer_business_model=mapping['JEL_Offer_Business_model__c'],
            jel_offer_pct_compensation=mapping['JEL_Offer_Compensation__c'],
            jel_offer_pct_fenced_discount=mapping['JEL_Offer_Fenced_discount__c'],
            jel_offer_pct_base_allocation=mapping['JEL_Offer_Base_allocation__c'],
            jel_approach_type=mapping['JEL_Approach_type__c'],
            jel_priority_level=mapping['JEL_Priority_level__c'],
            jel_expedia_lead_owner=mapping['Join_Expedia_Lead_Owner_Name__c'],
            lead_owner_email=mapping['Lead_Owner_Email__c'],
            sf_submarket_id=submarket_result['Id'],
            jel_mapping=mapping
        )

    sql_db_connection.close()


def validate_fields(data_object):
    """
    This validates that all the required fields exist in the data being sent
    to the service.

    Args:
        data_object (dict): The object to confirm the fields exist in.

    Returns:
        bool, str: The boolean value is True if the validation passes, or False
            false if it fails. The message is blank if validation passes, or
            will contain a message if the validation fails.
    """
    validation_pass = True
    validation_message = ''
    missing_fields = []

    # [['numberOfRooms', 'numberOfProperties'], 'submarketId', 'addressCountryCode']
    submarket_id = data_object.get('submarketId')
    address_country_code = data_object.get('addressCountryCode')
    num_rooms = data_object.get('numberOfRooms')
    num_properties = data_object.get('numberOfProperties')

    if not submarket_id:
        missing_fields.append('submarketId')

    if not address_country_code:
        missing_fields.append('addressCountryCode')

    if not num_rooms and not num_properties:
        missing_fields.append('numberOfRooms')
        missing_fields.append('numberOfProperties')
    elif not num_rooms and num_properties <= 1:
        missing_fields.append('numberOfProperties must be > 1')

    if len(missing_fields) > 0:
        validation_message = 'One or more required fields are missing: '
        validation_message += ",".join(missing_fields)
        validation_pass = False

    return validation_pass, validation_message


def generate_result(**kwargs):
    """
    Generates the result message to be returned back to the api gateway.

    Args:
        kwargs:
            status_code (int): The HTTP status code to return
            headers: Any headers that need to be returned
            result_message (str): Any string message you want returned to the
                end user. If the processing was successful, this will just be
                "success". If there was an error, this will display the error
                message.
            jel_offer_business_model: from the matched jel mapping record
            jel_offer_pct_compensation: from the matched jel mapping record
            jel_offer_pct_fenced_discount: from the matched jel mapping record
            jel_offer_pct_base_allocation: from the matched jel mapping record
            jel_approach_type: from the matched jel mapping record
            jel_priority_level: from the matched jel mapping record
            jel_expedia_lead_owner: from the matched jel mapping record
            lead_owner_email: from the matched jel mapping record
            jel_mapping: The actual mapping record. This is added to the result
                so it can be forwarded to the next api. This is done as a
                complete package to make it easier for any future changes that
                need to be done end to end for us without involving EPCO.

    Returns:
        dict: Returns an object with status code (default 200), headers, None,
            and a body that contains all the details needed with the terms. If
            there is an error, the error message will be in the result_message
            field of the body
    """
    result_message = {}
    result_message['statusCode'] = kwargs.get('status_code', 200)
    result_message['headers'] = kwargs.get('headers', {'Content-Type': 'application/json'})
    result_message['isBase64Encoded'] = kwargs.get('isBase64Encoded', False)

    result_body = {}
    result_body['resultMessage'] = kwargs.get('result_message', '')

    if 'jel_offer_business_model' in kwargs:
        result_body['jelOfferBusinessModel'] = kwargs.get('jel_offer_business_model', '')
    if 'jel_offer_pct_compensation' in kwargs:
        result_body['jelOfferPctCompensation'] = kwargs.get('jel_offer_pct_compensation', '')
    if 'jel_offer_pct_fenced_discount' in kwargs:
        result_body['jelOfferPctFencedDiscount'] = kwargs.get('jel_offer_pct_fenced_discount', '')
    if 'jel_offer_pct_base_allocation' in kwargs:
        result_body['jelOfferPctBaseAllocation'] = kwargs.get('jel_offer_pct_base_allocation', '')
    if 'jel_approach_type' in kwargs:
        result_body['jelApproachType'] = kwargs.get('jel_approach_type', '')
    if 'jel_priority_level' in kwargs:
        result_body['jelPriorityLevel'] = kwargs.get('jel_priority_level', '')
    if 'jel_expedia_lead_owner' in kwargs:
        result_body['joinExpediaLeadOwner'] = kwargs.get('jel_expedia_lead_owner', '')
    if 'lead_owner_email' in kwargs:
        result_body['leadOwnerEmail'] = kwargs.get('lead_owner_email', '')
    if 'sf_submarket_id' in kwargs:
        result_body['sfSubmarketId'] = kwargs.get('sf_submarket_id', '')
    if 'jel_mapping' in kwargs:
        jel_mapping = kwargs.get('jel_mapping', {})
        jel_mapping["Active__c"] = str(jel_mapping["Active__c"]) if jel_mapping["Active__c"] else None
        jel_mapping["Room_Maximum__c"] = str(jel_mapping["Room_Maximum__c"]) if jel_mapping["Room_Maximum__c"] else None
        jel_mapping["Room_Minimum__c"] = str(jel_mapping["Room_Minimum__c"]) if jel_mapping["Room_Minimum__c"] else None
        jel_mapping["Property_Maximum__c"] = str(jel_mapping["Property_Maximum__c"]) if jel_mapping["Property_Maximum__c"] else None
        jel_mapping["Property_Minimum__c"] = str(jel_mapping["Property_Minimum__c"]) if jel_mapping["Property_Minimum__c"] else None
        jel_mapping["Do_Not_CC_JE_Lead_Owner__c"] = str(jel_mapping["Do_Not_CC_JE_Lead_Owner__c"]) if jel_mapping["Do_Not_CC_JE_Lead_Owner__c"] else None
        jel_mapping["JEL_Offer_Base_allocation__c"] = str(jel_mapping["JEL_Offer_Base_allocation__c"]) if jel_mapping["JEL_Offer_Base_allocation__c"] else None
        jel_mapping["JEL_Offer_Business_model__c"] = str(jel_mapping["JEL_Offer_Business_model__c"]) if jel_mapping["JEL_Offer_Business_model__c"] else None
        jel_mapping["JEL_Offer_Compensation__c"] = str(jel_mapping["JEL_Offer_Compensation__c"]) if jel_mapping["JEL_Offer_Compensation__c"] else None
        jel_mapping["JEL_Offer_Fenced_discount__c"] = str(jel_mapping["JEL_Offer_Fenced_discount__c"]) if jel_mapping["JEL_Offer_Fenced_discount__c"] else None
        jel_mapping["JEL_Priority_level__c"] = str(jel_mapping["JEL_Priority_level__c"]) if jel_mapping["JEL_Priority_level__c"] else None
        result_body['jelMapping'] = kwargs.get('jel_mapping', {})

    result_message['body'] = result_body

    return result_message


# if __name__ == "__main__":
#     # AWS settings

#     os.environ["service_region"] = 'us-west-2'
#     os.environ["config_table_name"] = 'app_configuration'
#     os.environ["config_table_primary_key"] = 'configuration_name'
#     os.environ['db_config'] = 'jel_mapping_rds_localhost_v2'
#     # os.environ["db_config"] = "jel_mapping_rds_staging_v2"

#     # test data
#     join_data = {'submarketId':'123887', 'numberOfRooms':5, 'numberOfProperties':1,'addressCountryCode':'USA'}

#     get_results = lambda_handler(join_data, '')
#     print('get_results: {}'.format(get_results))