import os
import sys
sys.path.append("{}/pip_libs".format(os.path.dirname(os.path.realpath(__file__))))
import traceback
import json

import boto3


def lambda_handler(event, context):
    sync_exception_sqs_url = os.environ["sync_exception_sqs_url"]
    sync_sns_arn = os.environ["sync_sns_arn"]

    async_exception_sqs_url = os.environ["async_exception_sqs_url"]
    async_sns_arn = os.environ["async_sns_arn"]

    try:
        rerun_jobs(sync_exception_sqs_url, sync_sns_arn, async_exception_sqs_url, async_sns_arn)
        return generate_result()
    except Exception as ex:    
        exception_message = ("error trying to rerun exceptions:\n\n" + 
            "exception:\n" + str(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)))

        print("exception: {}".format(exception_message))

        return generate_result(status_code=500, result_message=exception_message)


def rerun_jobs(sync_exception_sqs_url, sync_sns_arn, async_exception_sqs_url, async_sns_arn):
    """
    Takes the exceptions from the exception queues and pushes them to sns 
    messages to rerun the sync and async jobs to create the Salesforce 
    records.

    Args:
        sync_exception_sqs_url (str): The sqs for the sync exceptions.

    """
    print("Rerunning exceptions.")

    # clear out the async exception queue
    while True:
        print("in loop to get async exceptions.")
        sqs_items = get_sqs_items(async_exception_sqs_url, 10)

        if sqs_items and "Messages" in sqs_items:
            for message in sqs_items["Messages"]:
                error_obj = json.loads(message["Body"])
                event_message = json.dumps(error_obj["event"])
                send_to_sns(async_sns_arn, event_message)
                clear_sqs_item(async_exception_sqs_url, message["ReceiptHandle"])
        else:
            break

    # clear out the sync exception queue
    while True:
        print("in loop to get sync exceptions.")
        sqs_items = get_sqs_items(sync_exception_sqs_url, 10)

        if sqs_items and "Messages" in sqs_items:
            for message in sqs_items["Messages"]:
                error_obj = json.loads(message["Body"])
                event_message = json.dumps(error_obj["event"])
                send_to_sns(sync_sns_arn, event_message)
                clear_sqs_item(sync_exception_sqs_url, message["ReceiptHandle"])
        else:
            break


def get_sqs_items(sqs_url, max_messages):
    print("getting sqs items from {}.".format(sqs_url))
    sqs = boto3.client('sqs')
    response = sqs.receive_message(
        QueueUrl=sqs_url,
        MaxNumberOfMessages=max_messages
    )
    return response


def clear_sqs_item(sqs_url, receipt_handle):
    print("clearing message {} from {}".format(receipt_handle, sqs_url))
    sqs = boto3.client('sqs')
    response = sqs.delete_message(
        QueueUrl=sqs_url,
        ReceiptHandle=receipt_handle
    )
    return response


def send_to_sns(sns_arn, sns_message):
    """
    Send this message into the sns

    Args:
        sns_arn (str): the aws arn for the sns
        sns_message (str): The message to be pushed into the sns

    Returns
        dict: The response from aws publishing the message
    """
    print("sending {} message to sns {}.".format(sns_message, sns_arn))
    sns = boto3.client('sns')
    response = sns.publish(
        TopicArn=sns_arn,
        Message=json.dumps(sns_message)
    )
    return response

def generate_result(**kwargs):
    result_message = {}
    result_message['statusCode'] = kwargs.get('status_code', 200)
    result_message['headers'] = kwargs.get('headers', None)

    result_body = {}
    result_body['resultMessage'] = kwargs.get('result_message', 'success')    

    result_message['body'] = result_body

    return result_message


if __name__ == "__main__":
    os.environ["sync_exception_sqs_url"] = "https://sqs.us-west-2.amazonaws.com/198743346998/join-expedia-sync-insert-exceptions.fifo"
    os.environ["sync_sns_arn"] = "arn:aws:sns:us-west-2:198743346998:join-expedia-sync-inserts"

    os.environ["async_exception_sqs_url"] = "https://sqs.us-west-2.amazonaws.com/198743346998/join-expedia-async-insert-exceptions.fifo"
    os.environ["async_sns_arn"] = "arn:aws:sns:us-west-2:198743346998:join-expedia-async-inserts"

    rerun_results = lambda_handler(None, None)
    print("rerun_results: {}".format(rerun_results))