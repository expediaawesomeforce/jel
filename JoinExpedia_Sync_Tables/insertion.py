from db_schema import language, country, join_case, submarket, join_expedia_lead_email_mapping
from db_schema import db

def country_add(insertList):
    countryList = []
    for item in insertList:
        c = country()
        c.Id = item['Id']
        if 'Name' in item:
            c.Name = item['Name']
        if 'ISO_3_country_code__c' in item:
            c.ISO_3_country_code__c = item['ISO_3_country_code__c']
        countryList.append(c)

    if countryList:
        db.session.add_all(countryList)
        db.session.commit()


def language_add(insertList):
    languageList = []
    for item in insertList:
        lan = language()
        lan.Id = item['Id']
        if 'Name' in item:
            lan.Name = item['Name']
        languageList.append(lan)

    if languageList:
        db.session.add_all(languageList)
        db.session.commit()

def join_case_add(insertList):
    join_caseList = []
    for item in insertList:
        jc = join_case()

        jc.Id = item['Id']
        if 'CreatedDate' in item:
            jc.CreatedDate = item['CreatedDate']
        if 'ContactId' in item:
            jc.ContactId = item['ContactId']
        if 'AccountId' in item:
            jc.AccountId = item['AccountId']
        if 'Join_Last_Name__c' in item:
            jc.Join_Last_Name__c = item['Join_Last_Name__c']
        if 'Join_Phone__c' in item:
            jc.Join_Phone__c = item['Join_Phone__c']
        if 'Join_DBA_Name_Doing_Business_As__c' in item:
            jc.Join_DBA_Name_Doing_Business_As__c = item['Join_DBA_Name_Doing_Business_As__c']
        if 'Join_Street_Address__c' in item:
            jc.Join_Street_Address__c = item['Join_Street_Address__c']
        join_caseList.append(jc)

    if join_caseList:
        db.session.add_all(join_caseList)
        db.session.commit()

def submarket_add(insertList):
    submarketList = []
    for item in insertList:
        s = submarket()
        s.Id = item['Id']
        if 'Name' in item:
            s.Name = item['Name']
        if 'Active__c' in item:
            s.Active__c = item['Active__c']
        if 'Expedia_Sub_Market_Id__c' in item:
            s.Expedia_Sub_Market_Id__c = item['Expedia_Sub_Market_Id__c']
        if 'Market__c' in item:
            s.Market__c = item['Market__c']
        if 'Region__c' in item:
            s.Region__c = item['Region__c']
        if 'Super_Region__c' in item:
            s.Super_Region__c = item['Super_Region__c']

        submarketList.append(s)

    if submarketList:
        db.session.add_all(submarketList)
        db.session.commit()

def join_expedia_lead_email_mapping_add(insertList):
    join_expedia_lead_email_mappingList = []
    for item in insertList:
        so = join_expedia_lead_email_mapping()
        so.Id = item['Id']
        if 'Name' in item:
            so.Name = item['Name']
        if 'Active__c' in item:
            so.Active__c = item['Active__c']
        if 'CC_Addresses__c' in item:
            so.CC_Addresses__c = item['CC_Addresses__c']
        if 'Email_Template_Id__c' in item:
            so.Email_Template_Id__c = item['Email_Template_Id__c']
        if 'Market__c' in item:
            so.Market__c = item['Market__c']
        if 'MMA__c' in item:
            so.MMA__c = item['MMA__c']
        if 'MAA__c' in item:
            so.MAA__c = item['MAA__c']
        if 'Room_Maximum__c' in item:
            so.Room_Maximum__c = item['Room_Maximum__c']
        if 'Room_Minimum__c' in item:
            so.Room_Minimum__c = item['Room_Minimum__c']
        if 'Property_Minimum__c' in item:
            so.Property_Minimum__c = item['Property_Minimum__c']
        if 'Property_Maximum__c' in item:
            so.Property_Maximum__c = item['Property_Maximum__c']
        if 'Rule_Description__c' in item:
            so.Rule_Description__c = item['Rule_Description__c']
        if 'From_Org_Wide_Email_Id__c' in item:
            so.From_Org_Wide_Email_Id__c = item['From_Org_Wide_Email_Id__c']
        if 'Submarket__c' in item:
            so.Submarket__c = item['Submarket__c']
        if 'Commercial_Conditions__c' in item:
            so.Commercial_Conditions__c = item['Commercial_Conditions__c']
        if 'Do_Not_CC_JE_Lead_Owner__c' in item:
            so.Do_Not_CC_JE_Lead_Owner__c = item['Do_Not_CC_JE_Lead_Owner__c']
        if 'Email_Template_Name__c' in item:
            so.Email_Template_Name__c = item['Email_Template_Name__c']
        if 'From_Org_Wide_Email_Address__c' in item:
            so.From_Org_Wide_Email_Address__c = item['From_Org_Wide_Email_Address__c']
        if 'Join_Country_ISO_3_Code__c' in item:
            so.Join_Country_ISO_3_Code__c = item['Join_Country_ISO_3_Code__c']
        if 'Join_Country_Lookup__c' in item:
            so.Join_Country_Lookup__c = item['Join_Country_Lookup__c']
        if 'Join_Expedia_Lead_Owner__c' in item:
            so.Join_Expedia_Lead_Owner__c = item['Join_Expedia_Lead_Owner__c']
        if 'Join_Expedia_Lead_Owner_Name__c' in item:
            so.Join_Expedia_Lead_Owner_Name__c = item['Join_Expedia_Lead_Owner_Name__c']
        if 'Region__c' in item:
            so.Region__c = item['Region__c']
        if 'Lead_Owner_Email__c' in item:
            so.Lead_Owner_Email__c = item['Lead_Owner_Email__c']
        if 'Template_Type__c' in item:
            so.Template_Type__c = item['Template_Type__c']
        if 'JEL_Approach_type__c' in item:
            so.JEL_Approach_type__c = item['JEL_Approach_type__c']
        if 'JEL_Offer_Base_allocation__c' in item:
            so.JEL_Offer_Base_allocation__c = item['JEL_Offer_Base_allocation__c']
        if 'JEL_Offer_Business_model__c' in item:
            so.JEL_Offer_Business_model__c = item['JEL_Offer_Business_model__c']
        if 'JEL_Offer_Compensation__c' in item:
            so.JEL_Offer_Compensation__c = item['JEL_Offer_Compensation__c']
        if 'JEL_Offer_Fenced_discount__c' in item:
            so.JEL_Offer_Fenced_discount__c = item['JEL_Offer_Fenced_discount__c']
        if 'JEL_Priority_level__c' in item:
            so.JEL_Priority_level__c = item['JEL_Priority_level__c']
        if 'RecordTypeName__c' in item:
            so.RecordTypeDeveloperName = item['RecordTypeName__c']

        join_expedia_lead_email_mappingList.append(so)

    if join_expedia_lead_email_mappingList:
        db.session.add_all(join_expedia_lead_email_mappingList)
        db.session.commit()