import insertion
import deletion
import json
import boto3
import datetime
import os

def dbservice(event, context):
    #event =  [{ "attributes":{ "type":"Country__c","url":"/services/data/v41.0/sobjects/Country__c/a0B5B000006pnf2UAA"},"IsDeleted":False, "LastModifiedDate":"2017-10-23T19:18:12.000+0000","ETP_exception__c":"Exclude","OwnerId":"0051A000009dHjgQAE1", "CreatedById":"0051A000009dHjgQAE","ISO_3_country_code__c":"i2","CreatedDate":"2017-10-23T19:18:12.000+0000","Id":"a0B5B000006pnf2UAd","LastModifiedById":"0051A000009dHjgQAE","ISO_2_country_code__c":"i2","Name":"test3","SystemModstamp":"2017-10-23T19:18:12.000+0000"},{"attributes":{ "type":"Country__c","url":"/services/data/v41.0/sobjects/Country__c/a0B5B000006pnf2UAA"},"IsDeleted":False, "LastModifiedDate":"2017-10-23T19:18:12.000+0000","ETP_exception__c":"Exclude","OwnerId":"0051A000009dHjgQAE1", "CreatedById":"0051A000009dHjgQAE","ISO_3_country_code__c":"i2","CreatedDate":"2017-10-23T19:18:12.000+0000","Id":"a0B5B000006pnf2UAc","LastModifiedById":"0051A000009dHjgQAE","ISO_2_country_code__c":"i2","Name":"test34","SystemModstamp":"2017-10-23T19:18:12.000+0000"}]
    print(event)
    if 'body' in event:
        try:
            event = json.loads(event['body'])
        except Exception as e:
            print(e)
            return response('error with request body, contact your administrator', 400)

    print(event)
    for item in event:
        print(item)
        if "Id" not in item:
            print(item)
            return response('Id - attribute is missing for a record, so not able to process your request ' + item, 400)

    insertList = []
    deleteList = []
    for item in event:
        if "IsDeleted" in item and not item["IsDeleted"]:
            insertList.append(item)
        if "IsDeleted" in item and item["IsDeleted"]:
            deleteList.append(item)

    if insertList:
        insert(insertList)
    if deleteList:
        delete(deleteList)
    return response('success', 200)


def response(body, statuscode):
    return {"isBase64Encoded": False, "statusCode": statuscode, "body": body}

'''
    To insert the record into db this code chesks if this record with this ID already available or not
      if it exists it gets deleted and inserts this record into db(an update scenerio).
'''
def insert(item):
    print(item[0]["attributes"]["type"])

    if item[0]["attributes"]["type"].lower() == "submarket__c":
        deletion.submarket_del(item)
        insertion.submarket_add(item)
    elif item[0]["attributes"]["type"].lower() == "join_expedia_lead_email_mapping__c":
        deletion.join_expedia_lead_email_mapping_del(item)
        insertion.join_expedia_lead_email_mapping_add(item)
    elif item[0]["attributes"]["type"].lower() == "country__c":
        deletion.country_del(item)
        insertion.country_add(item)
    elif item[0]["attributes"]["type"].lower() == "language__c":
        deletion.language_del(item)
        insertion.language_add(item)

def delete(item):
    print(item[0]["attributes"]["type"])

    if item[0]["attributes"]["type"].lower() == "submarket__c":
        deletion.submarket_del(item)
    elif item[0]["attributes"]["type"].lower() == "join_expedia_lead_email_mapping__c":
        deletion.join_expedia_lead_email_mapping_del(item)
    elif item[0]["attributes"]["type"].lower() == "country__c":
        deletion.country_del(item)
    elif item[0]["attributes"]["type"].lower() == "language__c":
        deletion.language_del(item)
