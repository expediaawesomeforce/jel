import os
import sys
sys.path.append("{}/pip_libs".format(os.path.dirname(os.path.realpath(__file__))))
import traceback
import dateutil.parser

import pymysql

import database_helper
import sfdc_helper
import pysalesforce

def lambda_handler(event, context):
    # AWS settings
    service_region = os.environ["service_region"]
    config_table_name = os.environ["config_table_name"]
    config_table_primary_key = os.environ["config_table_primary_key"]
    db_config_table_record_id = os.environ['db_config']

    # Salesforce settings
    sf_config_record_id = os.environ["sf_config"]

    # app settings
    query_last_n_hours = os.environ.get('query_last_n_hours', None)
    truncate_table = bool(os.environ.get('truncate_table', None))
    drop_table = bool(os.environ.get('drop_table', None))

    try:
        sync_jel_mapping_to_sql(service_region, config_table_name, config_table_primary_key, db_config_table_record_id, sf_config_record_id, query_last_n_hours, drop_table, truncate_table, event)
    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        exception_message = ("error trying to rerun exceptions:\n\n" + "exception:\n" + str(traceback.format_exception(exc_type, exc_value, exc_traceback)))

        print("exception: {}".format(exception_message))


def sync_jel_mapping_to_sql(service_region, config_table_name, config_table_primary_key, db_config_id, sf_config_id, query_last_n_hours, drop_table, truncate_table, event_data):
    """
    This method pulls the latest data from Salesforce and upates the tables 
    in a local mysql table so they can be quickly queried without having to
    make a Salesforce callout.

    Args:
        service_region (str): The AWS Service Region. e.g. us-west-2
        config_table_name (strJoin_Expedia_Lead_Owner_Name__c): Name of configuration table storing the
                                 encrypted configs
        config_table_primary_key (str): Name of primary key for the 
                                        configuration table
        db_config_id (str): The primary key for the record you want the 
                            configuration details for. In this case this 
                            is the configuration for the MySQL credentials.
        sf_config_id (str): The primary key for the record you want the 
                            configuration details for. In this case this
                            is the configuraiton for the Salesforce 
                            credentials
        query_last_n_hours (str): This limits the number of hours to query
                                  for changed records. For example if this 
                                  is set to '1', it will query the 
                                  Salesforce records modified in the last
                                  hour. If this is left blank, it will
                                  query for all records.
        drop_table (bool): If this is true, the local copies of the 
                           requested table will be dropped.
        truncate_table (bool): If this is true, the local copies of the 
                               submarket and jel mapping tables will be
                               truncated so they can be rebuilt.

    Returns:
        None
    """
    table_configurations = {
        'submarket': {
            'sfdc_object_name': 'Submarket__c',
            'sql_table_name': 'submarket',
            'fields': [
                {'sfdc_field_name': 'Id', 'sql_field_name': 'Id', 'sql_field_properties': 'VARCHAR(18) NOT NULL UNIQUE', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Name', 'sql_field_name': 'Name', 'sql_field_properties': 'VARCHAR(80)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Active__c', 'sql_field_name': 'Active__c', 'sql_field_properties': 'TINYINT', 'sf_data_type': 'boolean'},
                {'sfdc_field_name': 'Expedia_Sub_Market_Id__c', 'sql_field_name': 'Expedia_Sub_Market_Id__c', 'sql_field_properties': 'VARCHAR(10)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Market__c', 'sql_field_name': 'Market__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Market__r.Region__c', 'sql_field_name': 'Region__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Market__r.Region__r.Super_Region__c', 'sql_field_name': 'Super_Region__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'}
            ]
        },
        'join_expedia_lead_email_mapping': {
            'sfdc_object_name': 'Join_Expedia_Lead_Email_Mapping__c',
            'sql_table_name': 'join_expedia_lead_email_mapping',
            'fields': [
                {'sfdc_field_name': 'Id', 'sql_field_name': 'Id', 'sql_field_properties': 'VARCHAR(18) NOT NULL UNIQUE', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Name', 'sql_field_name': 'Name', 'sql_field_properties': 'VARCHAR(80)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Active__c', 'sql_field_name': 'Active__c', 'sql_field_properties': 'TINYINT', 'sf_data_type': 'boolean'},
                {'sfdc_field_name': 'CC_Addresses__c', 'sql_field_name': 'CC_Addresses__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Email_Template_Id__c', 'sql_field_name': 'Email_Template_Id__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Market__c', 'sql_field_name': 'Market__c', 'sql_field_properties': 'VARCHAR(18)',  'sf_data_type': 'id'},
                {'sfdc_field_name': 'MMA__c', 'sql_field_name': 'MMA__c', 'sql_field_properties': 'VARCHAR(18)',  'sf_data_type': 'id'},
                {'sfdc_field_name': 'MAA__c', 'sql_field_name': 'MAA__c', 'sql_field_properties': 'VARCHAR(18)',  'sf_data_type': 'id'},
                {'sfdc_field_name': 'Room_Maximum__c', 'sql_field_name': 'Room_Maximum__c', 'sql_field_properties': 'INT', 'sf_data_type': 'numerical'},
                {'sfdc_field_name': 'Room_Minimum__c', 'sql_field_name': 'Room_Minimum__c', 'sql_field_properties': 'INT', 'sf_data_type': 'numerical'},
                {'sfdc_field_name': 'Property_Minimum__c', 'sql_field_name': 'Property_Minimum__c', 'sql_field_properties': 'INT', 'sf_data_type': 'numerical'},
                {'sfdc_field_name': 'Property_Maximum__c', 'sql_field_name': 'Property_Maximum__c', 'sql_field_properties': 'INT', 'sf_data_type': 'numerical'},
                {'sfdc_field_name': 'Rule_Description__c',  'sql_field_name': 'Rule_Description__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'From_Org_Wide_Email_Id__c', 'sql_field_name': 'From_Org_Wide_Email_Id__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Submarket__c', 'sql_field_name': 'Submarket__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Commercial_Conditions__c', 'sql_field_name': 'Commercial_Conditions__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Do_Not_CC_JE_Lead_Owner__c', 'sql_field_name': 'Do_Not_CC_JE_Lead_Owner__c', 'sql_field_properties': 'TINYINT', 'sf_data_type': 'boolean'},
                {'sfdc_field_name': 'Email_Template_Name__c', 'sql_field_name': 'Email_Template_Name__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'From_Org_Wide_Email_Address__c', 'sql_field_name': 'From_Org_Wide_Email_Address__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Join_Country_ISO_3_Code__c', 'sql_field_name': 'Join_Country_ISO_3_Code__c', 'sql_field_properties': 'VARCHAR(3)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Join_Country_Lookup__c', 'sql_field_name': 'Join_Country_Lookup__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Join_Expedia_Lead_Owner__c', 'sql_field_name': 'Join_Expedia_Lead_Owner__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Join_Expedia_Lead_Owner__r.Name', 'sql_field_name': 'Join_Expedia_Lead_Owner_Name__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Lead_Owner_Email__c', 'sql_field_name': 'Lead_Owner_Email__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Region__c', 'sql_field_name': 'Region__c', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Template_Type__c', 'sql_field_name': 'Template_Type__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'JEL_Approach_type__c', 'sql_field_name': 'JEL_Approach_type__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'JEL_Offer_Base_allocation__c', 'sql_field_name': 'JEL_Offer_Base_allocation__c', 'sql_field_properties': 'DECIMAL(10,5)', 'sf_data_type': 'numerical'},
                {'sfdc_field_name': 'JEL_Offer_Business_model__c', 'sql_field_name': 'JEL_Offer_Business_model__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'JEL_Offer_Compensation__c', 'sql_field_name': 'JEL_Offer_Compensation__c', 'sql_field_properties': 'DECIMAL(10,5)', 'sf_data_type': 'numerical'},
                {'sfdc_field_name': 'JEL_Offer_Fenced_discount__c', 'sql_field_name': 'JEL_Offer_Fenced_discount__c', 'sql_field_properties': 'DECIMAL(10,5)', 'sf_data_type': 'numerical'},
                {'sfdc_field_name': 'JEL_Priority_level__c', 'sql_field_name': 'JEL_Priority_level__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'RecordType.DeveloperName', 'sql_field_name': 'RecordTypeDeveloperName', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'}
            ]
        },
        'join_case': {
            'sfdc_object_name': 'Case',
            'sql_table_name': 'join_case',
            'where_clause': 'RecordType.DeveloperName = \'JoinLeads\' AND CreatedDate = LAST_N_DAYS:30',
            'fields': [
                {'sfdc_field_name': 'Id', 'sql_field_name': 'Id', 'sql_field_properties': 'VARCHAR(18) NOT NULL UNIQUE', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'CreatedDate', 'sql_field_name': 'CreatedDate', 'sql_field_properties': 'DATETIME(3)', 'sf_data_type': 'datetime'},
                {'sfdc_field_name': 'ContactId', 'sql_field_name': 'ContactId', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'AccountId', 'sql_field_name': 'AccountId', 'sql_field_properties': 'VARCHAR(18)', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Join_Last_Name__c', 'sql_field_name': 'Join_Last_Name__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Join_Phone__c', 'sql_field_name': 'Join_Phone__c', 'sql_field_properties': 'VARCHAR(40)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Join_DBA_Name_Doing_Business_As__c', 'sql_field_name': 'Join_DBA_Name_Doing_Business_As__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'Join_Street_Address__c', 'sql_field_name': 'Join_Street_Address__c', 'sql_field_properties': 'VARCHAR(255)', 'sf_data_type': 'string'}
            ]
        },
        'country': {
            'sfdc_object_name': 'Country__c',
            'sql_table_name': 'country',
            'fields': [
                {'sfdc_field_name': 'Id', 'sql_field_name': 'Id', 'sql_field_properties': 'VARCHAR(18) NOT NULL UNIQUE', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Name', 'sql_field_name': 'Name', 'sql_field_properties': 'VARCHAR(80)', 'sf_data_type': 'string'},
                {'sfdc_field_name': 'ISO_3_country_code__c', 'sql_field_name': 'ISO_3_country_code__c', 'sql_field_properties': 'VARCHAR(3)', 'sf_data_type': 'string'},
            ]
        },
        'language': {
            'sfdc_object_name': 'Language__c',
            'sql_table_name': 'language',
            'fields': [
                {'sfdc_field_name': 'Id', 'sql_field_name': 'Id', 'sql_field_properties': 'VARCHAR(18) NOT NULL UNIQUE', 'sf_data_type': 'id'},
                {'sfdc_field_name': 'Name', 'sql_field_name': 'Name', 'sql_field_properties': 'VARCHAR(80)', 'sf_data_type': 'string'}
            ]
        }
    }

    table_configuration = table_configurations.get(event_data.get("table_name", ''))

    print("starting process to sync table {}".format(table_configuration['sfdc_object_name']))

    print("logging in to salesforce.")
    sfdc_login_response = sfdc_helper.get_salesforce_token(service_region, config_table_name, config_table_primary_key, sf_config_id)
    sfdc_access_token = sfdc_login_response.get('access_token')
    sfdc_instance_url = sfdc_login_response.get('instance_url')

    if sfdc_instance_url is None:
        print("error logging into Salesforce: {}".format(sfdc_login_response))
        return None

    sql_db_connection = database_helper.get_rdb_client(service_region, config_table_name, config_table_primary_key, db_config_id)
    db_cursor = sql_db_connection.cursor(pymysql.cursors.DictCursor)

    # confirm whether or not the table can be found in the sql db
    check_table_exists = database_helper.check_table_exists(sql_db_connection, table_configuration["sql_table_name"])

    # if the table exists run the drop or truncate statements if they are needed
    if check_table_exists:
        if drop_table:
            print("dropping the sql table.")
            drop_statement = "DROP TABLE " + table_configuration["sql_table_name"]
            db_cursor.execute(drop_statement)
            check_table_exists = False
        elif truncate_table:
            print("truncating the table.")
            truncate_statement = "TRUNCATE " + table_configuration["sql_table_name"]
            db_cursor.execute(truncate_statement)
    
    # if the table doesn't exist, create it
    if not check_table_exists:
        print("creating the {} table in sql db.".format(table_configuration["sql_table_name"]))
        create_statement = database_helper.build_create_table_string(table_configuration)
        db_cursor.execute(create_statement)

    print("querying {} records.".format(table_configuration["sfdc_object_name"]))
    # build the string that will be used to query salesforce
    sfdc_query_string = sfdc_helper.build_sf_query_string(table_configuration)

    # if we only want to pull last_n_hours, then add to the query
    if query_last_n_hours is not None:
        n_hours_int = int(query_last_n_hours)
        last_modified_time = sfdc_helper.get_datetime_minus_hours(n_hours_int)

        if "WHERE" in sfdc_query_string:
            sfdc_query_string += " AND LastModifiedDate >= " + last_modified_time
        else:
            sfdc_query_string += " WHERE LastModifiedDate >= " + last_modified_time

    query_results = pysalesforce.Standard.query(sfdc_query_string, sfdc_access_token, sfdc_instance_url)
    print("query_results: {}".format(query_results))

    query_done = query_results.get("done")
    if query_done is None:
        print("error querying salesforce: {}".format(query_results))
        return None

    if not query_results["done"]:
        sfdc_helper.query_more_records(sfdc_access_token, sfdc_instance_url, query_results)

    # upsert records to the db
    record_count = len(query_results["records"])
    print("upserting {} records to {} sql db.".format(record_count, table_configuration["sql_table_name"]))
    values_list = []
    counter = 0
    insert_size = 500

    # get the base upsert string with {0} placeholder for the values
    upsert_string = database_helper.build_upsert_string(table_configuration)

    # loop through the salesforce records and perform the upserts
    for record in query_results["records"]:
        counter += 1

        field_items = []
        for field in table_configuration["fields"]:
            field_value = sfdc_helper.get_parent_value(record, field["sfdc_field_name"])

            # based on the field type, create the value to be inserted
            if field["sf_data_type"] == "id":
                escaped_field_value = "'" + field_value + "'" if field_value is not None else 'NULL'
            elif field["sf_data_type"] == "string":
                escaped_field_value = sql_db_connection.escape(field_value) if field_value is not None else 'NULL'
            elif field["sf_data_type"] == "boolean":
                escaped_field_value = 1 if field_value else 0
            elif field["sf_data_type"] == "numerical":
                escaped_field_value = field_value if field_value is not None else 'NULL'
            elif field["sf_data_type"] == "datetime":
                escaped_field_value = "'" + dateutil.parser.parse(field_value).strftime('%Y-%m-%d %H:%M:%S.000') + "'" if field_value is not None else 'NULL'
            elif field["sf_data_type"] == "date":
                escaped_field_value = "'" + field_value + "'" if field_value is not None else 'NULL'

            field_items.append(str(escaped_field_value))

        # join the field values together to create the single field item that will 
        # be upserted.
        field_item_string = ",".join(field_items)

        this_value = "(" + field_item_string + ")"

        values_list.append(this_value)

        # if the list is at or beyond the max insert size, then run the upsert
        if len(values_list) >= insert_size:
            values_list_string = ",".join(values_list)
            record_upsert = upsert_string.format(values_list_string)

            counter = 0
            values_list = []

            try:
                db_cursor.execute(record_upsert)
            except Exception as exception:
                print("\nexception hit: {}\nupsert request: {}".format(exception, record_upsert))
                return None

    # upsert any additional records that are still in the values list
    if len(values_list) > 0:
        values_list_string = ",".join(values_list)
        record_upsert = upsert_string.format(values_list_string)

        try:
            db_cursor.execute(record_upsert)
        except Exception as exception:
            print("\nexception hit: {}\nupsert request: {}".format(exception, record_upsert))
            return None

    # commit the changes to the db and close the connection
    sql_db_connection.commit()
    sql_db_connection.close()
    print("table sync complete.")


# if __name__ == "__main__":
#     # AWS settings
#     os.environ["service_region"] = 'us-west-2'
#     os.environ["config_table_name"] = 'app_configuration'
#     os.environ["config_table_primary_key"] = 'configuration_name'
#     os.environ['db_config'] = 'jel_mapping_rds_localhost_v2'
#     # os.environ["db_config"] = "jel_mapping_rds_staging_v2"

#     # Salesforce settings
#     os.environ["sf_config"] = 'salesforce_gsosfdc_staging_v2'

#     # app settings
#     # os.environ["truncate_table"] = 'True'
#     os.environ["drop_table"] = 'True'
#     # os.environ["query_last_n_hours"] = '1'

#     # options: 
#     # submarket
#     # join_expedia_lead_email_mapping
#     # join_case
#     # country
#     # language
#     lambda_event = {"table_name": "join_expedia_lead_email_mapping"}

#     lambda_handler(lambda_event, '')