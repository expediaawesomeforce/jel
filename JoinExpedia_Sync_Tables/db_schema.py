from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import pymysql

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ["dbURI"]
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://gsosfdc:tx4ak$A19GHR#6hL9NGN@joinexpedia.cinymya1oq4f.us-west-2.rds.amazonaws.com/JoinExpediaConfigurationMapping'
db = SQLAlchemy(app)

class submarket(db.Model):
    Id = db.Column(db.String(255), primary_key=True)
    Name = db.Column(db.String(255))
    Active__c = db.Column(db.Boolean())
    Expedia_Sub_Market_Id__c = db.Column(db.String(255))
    Market__c = db.Column(db.String(255))
    Region__c = db.Column(db.String(255))
    Super_Region__c = db.Column(db.String(255))

    def __repr__(self):
        return '<submarket %r' % self.Id

class join_expedia_lead_email_mapping(db.Model):
    Id = db.Column(db.String(255), primary_key=True)
    Name = db.Column(db.String(255))
    Active__c = db.Column(db.Boolean())
    CC_Addresses__c = db.Column(db.String(255))
    Email_Template_Id__c = db.Column(db.String(255))
    Market__c = db.Column(db.String(255))
    MMA__c = db.Column(db.String(255))
    MAA__c = db.Column(db.String(255))
    Room_Maximum__c = db.Column(db.Integer())
    Room_Minimum__c = db.Column(db.Integer())
    Property_Minimum__c = db.Column(db.Integer())
    Property_Maximum__c = db.Column(db.Integer())
    Rule_Description__c = db.Column(db.String(255))
    From_Org_Wide_Email_Id__c = db.Column(db.String(255))
    Submarket__c = db.Column(db.String(255))
    Commercial_Conditions__c = db.Column(db.String(255))
    Do_Not_CC_JE_Lead_Owner__c = db.Column(db.Boolean())
    Email_Template_Name__c = db.Column(db.String(255))
    From_Org_Wide_Email_Address__c = db.Column(db.String(255))
    Join_Country_ISO_3_Code__c = db.Column(db.String(255))
    Join_Country_Lookup__c = db.Column(db.String(255))
    Join_Expedia_Lead_Owner__c = db.Column(db.String(255))
    Join_Expedia_Lead_Owner_Name__c = db.Column(db.String(255))
    Lead_Owner_Email__c = db.Column(db.String(255))
    Region__c = db.Column(db.String(255))
    Template_Type__c = db.Column(db.String(255))
    JEL_Approach_type__c = db.Column(db.String(255))
    JEL_Offer_Base_allocation__c = db.Column(db.Float())
    JEL_Offer_Business_model__c = db.Column(db.String(255))
    JEL_Offer_Compensation__c = db.Column(db.Float())
    JEL_Offer_Fenced_discount__c = db.Column(db.Float())
    JEL_Priority_level__c = db.Column(db.String(255))
    RecordTypeDeveloperName = db.Column(db.String(255))

    def __repr__(self):
        return '<join_expedia_lead_email_mapping %r' % self.Id

class join_case(db.Model):
    Id = db.Column(db.String(255), primary_key=True)
    CreatedDate = db.Column(db.DateTime())
    ContactId = db.Column(db.String(255))
    AccountId = db.Column(db.String(255))
    Join_Last_Name__c = db.Column(db.String(255))
    Join_Phone__c = db.Column(db.String(255))
    Join_DBA_Name_Doing_Business_As__c = db.Column(db.String(255))
    Join_Street_Address__c = db.Column(db.String(255))

    def __repr__(self):
        return "<join_case %r" % self.Id

class country(db.Model):
    Id = db.Column(db.String(255), primary_key=True)
    Name = db.Column(db.String(255))
    ISO_3_country_code__c = db.Column(db.String(255))

    def __repr__(self):
        return '<country %r' % self.Id

class language(db.Model):
    Id = db.Column(db.String(255), primary_key=True)
    Name = db.Column(db.String(255))

    def __repr__(self):
        return '<language %r' % self.Id

db.create_all()


