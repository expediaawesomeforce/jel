from db_schema import language, country, join_case, submarket, join_expedia_lead_email_mapping
from db_schema import db

def country_del(deleteList):
    countryList = []
    for item in deleteList:
        request = country.query.filter_by(Id=item['Id']).first()
        print(request)
        if request is not None:
            countryList.append(request)
            db.session.delete(request)
    if countryList:
        db.session.commit()

def language_del(deleteList):
    languageList = []
    for item in deleteList:
        request = language.query.filter_by(Id=item['Id']).first()
        print(request)
        if request is not None:
            languageList.append(request)
            db.session.delete(request)
    if languageList:
        db.session.commit()

def join_case_del(deleteList):
    join_caseList = []
    for item in deleteList:
        request = join_case.query.filter_by(Id=item['Id']).first()
        print(request)
        if request is not None:
            join_caseList.append(request)
            db.session.delete(request)
    if join_caseList:
        db.session.commit()

def submarket_del(deleteList):
    submarketList = []
    for item in deleteList:
        request = submarket.query.filter_by(Id=item['Id']).first()
        print(request)
        if request is not None:
            submarketList.append(request)
            db.session.delete(request)
    if submarketList:
        db.session.commit()

def join_expedia_lead_email_mapping_del(deleteList):
    join_expedia_lead_email_mappingList = []
    for item in deleteList:
        request = join_expedia_lead_email_mapping.query.filter_by(Id=item['Id']).first()
        print(request)
        if request is not None:
            join_expedia_lead_email_mappingList.append(request)
            db.session.delete(request)
    if join_expedia_lead_email_mappingList:
        db.session.commit()
