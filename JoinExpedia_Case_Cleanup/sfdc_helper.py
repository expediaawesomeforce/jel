import json
from datetime import datetime, timedelta
from copy import deepcopy

from config import Config
import pysalesforce
import webservice


def get_salesforce_token(service_region, config_table_name, config_table_primary_key, config_table_record_id):
    """
    This method logs into Salesforce using OAuth2 password grant and returns the login response 
    received from Salesforce

    Args:
        service_region (str): The AWS Service Region. e.g. us-west-2
        config_table_name (str): Name of configuration table storing the 
                                 encrypted configs
        config_table_primary_key (str): Name of primary key for the 
                                        configuration table
        config_table_record_id (str): The primary key for the record you want 
                                      the configugration details for. In this
                                      case this is the configuration for the 
                                      Salesforce credentials.

    Returns:
        object: Salesforce OAuth2 password grant response containing the 
                security token and instance url
    """
    print("getting environment configs")
    env_config = Config.get_config(service_region, config_table_name, config_table_primary_key, config_table_record_id)

    login_username = env_config.get("username", "")
    login_password = env_config.get("password", "") + env_config.get("token", "")
    login_client_id = env_config.get("oauthClientId", "")
    login_client_secret = env_config.get("oauthClientSecret", "")
    is_production = True if env_config["isProduction"] == '1' else False

    # data for login
    print("logging into {} environment".format(config_table_record_id))
    login_response = pysalesforce.Authentication.get_oauth_login(login_username, login_password, login_client_id, login_client_secret, is_production)

    return login_response


def query_more_records(access_token, instance_url, sobject_results):
    """
    This function is used to query the next in a set of records from Salesforce 
    A Salesforce query will return a value called 'nextRecordsUrl' if there are 
    more records to retrieved for a given query. This loops and continues to 
    add values to the sobject_results dict until all values are retrieved from 
    Salesforce.

    Args:
        access_token (str): the access_token value from the Salesforce login
                            response
        instance_url (str): The instance_url value from the Salesforce login
                            response
        sobject_results (dict): The salesforce object that has more records 
                                that need to be retrieved from the 
                                nextRecordsUrl. The results are added to the 
                                sobject_results['records']
    """
    header_details = pysalesforce.Util.get_standard_header(access_token)

    while sobject_results['done'] is False:
        additional_records = webservice.Tools.get_http_response(instance_url + sobject_results['nextRecordsUrl'], header_details)
        json_response = json.loads(additional_records.text)

        sobject_results['done'] = json_response['done']

        if 'nextRecordsUrl' in json_response:
            sobject_results['nextRecordsUrl'] = json_response['nextRecordsUrl']
        else:
            del sobject_results['nextRecordsUrl']

        sobject_results['records'].extend(json_response['records'])


def get_datetime_minus_hours(n_hours):
    """
    Takes the current datetime and returns a Salesforce formatted DateTime 
    value minus the number of hours specified.

    Args:
        n_hours (int): The number of hours to subtract from the current 
                       DateTime

    Returns:
        str: The current UTC time minus n_hours in a Salesforce formatted 
             DateTime "yyyy-mm-ddThh:mm:ssz"
    """
    utc_time = datetime.utcnow() - timedelta(hours=1)
    formatted_time = utc_time.strftime("%Y-%m-%dT%H:%M:%Sz")

    return formatted_time


def build_sf_query_string(table_config):
    """
    This takes a table configuration json and converts it into a statement
    to query salesforce.

    Args:
        table_config (obj): Object containing the table configuration 
                            details coming from the lambda.

    Returns:
        str: Query that can be used to get the table from Salesforce.
    """
    query_string = "SELECT "

    field_list = []
    for field in table_config["fields"]:
        field_list.append(field["sfdc_field_name"])

    field_list_string = ",".join(field_list)

    query_string += field_list_string + " FROM " + table_config["sfdc_object_name"]

    if table_config.get("where_clause") is not None:
        query_string += " WHERE " + table_config.get("where_clause")

    return query_string


def get_parent_value(record, field):
    """
    This function climbs up the lookup relationships in Salesforce to return 
    a the final value needed in the chain. For example 
    ['Market__r', 'Region__c', 'Super_Region__c'] would return the Super
    Region record Id from the Submarket record.

    Args:
        record (dict): The salesforce record to get the parent value from
        field (str): The salesforce field to get. If the field has a . 
                     in the name, this traverses through the objects to 
                     get the final value in the field.

    Returns:
        String: If a value is found, this will return the value. If no value 
                is found this will return ''
    """
    value = deepcopy(record)

    try:
        if '.' in field:
            field_list = field.split('.')
            for this_field in field_list:
                value = value[this_field]
        else:
            value = value[field]
    except:
        value = None

    return value