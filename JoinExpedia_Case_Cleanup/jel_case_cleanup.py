import os
import sys
sys.path.append("{}/pip_libs".format(os.path.dirname(os.path.realpath(__file__))))
import traceback
import json

import pymysql

import database_helper
import sfdc_helper
import webservice
import pysalesforce


def lambda_handler(event, context):
    # AWS settings
    service_region = os.environ["service_region"]
    config_table_name = os.environ["config_table_name"]
    config_table_primary_key = os.environ["config_table_primary_key"]
    db_config_table_record_id = os.environ['db_config']
    sf_config_id = os.environ["sf_config"]

    try:
        clean_up_cases(service_region, config_table_name, config_table_primary_key, db_config_table_record_id)
    except Exception as ex:    
        exception_message = ("error trying to process AWS JoinExpedia_SFDC_Async_Inserts\n\n" + 
            "event data passed into job:\n" + json.dumps(event) + "\n\n" + 
            "exception:\n" + str(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)))

        exception_email = {
            "targetObjectId": os.environ["tech_issue_user"],
            "subject": "Exception encountered in JoinExpedia AWS Job",
            "saveAsActivity": False,
            "plainTextBody": exception_message
        }

        sfdc_login_response = sfdc_helper.get_salesforce_token(service_region, config_table_name, config_table_primary_key, sf_config_id)
        sfdc_access_token = sfdc_login_response.get('access_token')
        sfdc_instance_url = sfdc_login_response.get('instance_url')

        # email_result = send_email(sfdc_access_token, sfdc_instance_url, exception_email)
        print("exception: {}".format(exception_message))


def clean_up_cases(service_region, config_table_name, config_table_primary_key, db_config_table_record_id):
    # delete query to remove cases with null CreatedDate or CreatedDate older than 30 days
    delete_query = "DELETE FROM join_case WHERE (CreatedDate NOT BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()) OR CreatedDate IS NULL"

    db_client = database_helper.get_rdb_client(service_region, config_table_name, config_table_primary_key, db_config_table_record_id)
    db_cursor = db_client.cursor(pymysql.cursors.DictCursor)
    db_cursor.execute(delete_query)
    print("deleted {} rows.".format(db_cursor.rowcount))
    db_client.commit()
    db_client.close()


def send_email(sfdc_access_token, sfdc_instance_url, email_object):
    """
    Sends an email with the custom Salesforce email send web service and 
    returns the response from the service.

    Args:
        sfdc_access_token (str): The access token returned from the 
                                 Salesforce login response.
        sfdc_instance_url (str): the instance url returned from the 
                                 Salesforce login response.
        email_object (dict): The dict containing all the attributes
                             that will be used to send an email with 
                             the custom service.

    Returns:
        dict: Returns a a list object for each email sent. This will only
              be a single item since this sends a single email. The result
              will contain 2 fields: sendIsSuccess to say whether or not
              the email send was successful, and emailErrors which is a 
              string array with any errors encountered when trying to send
              the email.
    """
    email_service_uri = "/services/apexrest/SendEmailService/"
    header_details = pysalesforce.Util.get_standard_header(sfdc_access_token)

    email_object = {
        "emails": [email_object],
        "sendAllOrNothing": False
    }

    json_data_body = json.dumps(email_object, indent=4, separators=(',', ': '))
    response = webservice.Tools.post_http_response(sfdc_instance_url + email_service_uri, json_data_body, header_details)
    json_response = json.loads(response.text)

    return json_response


if __name__ == "__main__":
    # AWS settings
    os.environ["service_region"] = 'us-west-2'
    os.environ["config_table_name"] = 'app_configuration'
    os.environ["config_table_primary_key"] = 'configuration_name'
    os.environ['db_config'] = 'jel_mapping_rds_localhost_v2'
    # os.environ["db_config"] = "jel_mapping_rds_staging_v2"

    # Salesforce settings
    os.environ["sf_config"] = 'salesforce_gsosfdc_staging_v2'
    os.environ["tech_issue_user"] = "0051A000009GjJDQA0"

    lambda_handler(None, None)